package com.example.tekno.arab_project.Data.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.ILoadMore;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListModel;
import com.example.tekno.arab_project.Model.CompanyList.ItemAdapter;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListAPI;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListResponse;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListResponseData;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListRetrofitGET;
import com.example.tekno.arab_project.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyListActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = CompanyListActivity.class.getName();
    private Toolbar toolbar;
    private static final Integer FILTER_CODE = 100;

    private CompanyListResponse companyListResponse;
    private List<CompanyListModel> companyListModels;
    private List<CompanyListResponseData> companyListResponseDatas;
    private SharedPreferences sharedPref;

    private RecyclerView companyListRcy;
    private ItemAdapter itemAdapter;
    private LinearLayoutManager layoutManager;

    private TextView btn_filter;

    private String menuListId;
    private String menuListName;
    private String access;
    private String filterRate = "";
    private String filterSearchText = "";
    private Integer skip;
    private Integer take;

    private Util util;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_list);

        util = new Util(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            menuListId = bundle.getString("menuListId");
            menuListName = bundle.getString("menuListName");
        }
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(menuListName);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");

        companyListRcy = (RecyclerView) findViewById(R.id.companyListRcy);
        btn_filter = (TextView) findViewById(R.id.btn_filter);
        btn_filter.setOnClickListener(this);
        skip = Constants.SKIP;
        take = Constants.TAKE;
        companyList();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        Intent intent = FilterActivity.newInstance(this, filterRate, filterSearchText);
        startActivityForResult(intent, FILTER_CODE);
    }

    private void companyList() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        Util.setProgressDialog(progressDialog);

        CompanyListModel companyListModel = new CompanyListModel(menuListId, skip, take, filterRate, filterSearchText);
        CompanyListAPI companyListRetrofitGET = CompanyListRetrofitGET.getRetrofitCompanyList().create(CompanyListAPI.class);
        Call<CompanyListResponse> call = companyListRetrofitGET.companyList(access, companyListModel);
        call.enqueue(new Callback<CompanyListResponse>() {
            @Override
            public void onResponse(Call<CompanyListResponse> call, Response<CompanyListResponse> response) {
                companyListResponse = response.body();
                if (companyListResponse != null) {
                    companyListResponseDatas = companyListResponse.getResponseData();
                    companyListResponseDatasSET(companyListResponseDatas);
                    Util.dismisProgressDialog(progressDialog);
                } else {
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<CompanyListResponse> call, Throwable t) {
                Log.e(TAG, "CompanyListResponse Error:" + t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }

    private void companyListResponseDatasSET(final List<CompanyListResponseData> companyListResponseDatas) {

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayout.VERTICAL);
        companyListRcy.setLayoutManager(layoutManager);
        itemAdapter = new ItemAdapter(companyListRcy, this, companyListResponseDatas);
        companyListRcy.setAdapter(itemAdapter);

        itemAdapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if (companyListResponseDatas.size() >= 10) {
                    companyListResponseDatas.add(null);
                    itemAdapter.notifyItemInserted(companyListResponseDatas.size() - 1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            companyListResponseDatas.remove(companyListResponseDatas.size() - 1);
                            itemAdapter.notifyItemRemoved(companyListResponseDatas.size() - 1);
                            skip += take;
                            CompanyListModel companyListModel = new CompanyListModel(menuListId, skip, take, filterRate, filterSearchText);
                            CompanyListAPI companyListRetrofitGET = CompanyListRetrofitGET.getRetrofitCompanyList().create(CompanyListAPI.class);
                            Call<CompanyListResponse> call = companyListRetrofitGET.companyList(access, companyListModel);
                            call.enqueue(new Callback<CompanyListResponse>() {
                                @Override
                                public void onResponse(Call<CompanyListResponse> call, Response<CompanyListResponse> response) {
                                    companyListResponse = response.body();
                                    try {
                                        if (companyListResponse != null) {
                                            for (CompanyListResponseData assessmentData : companyListResponse.getResponseData()) {
                                                companyListResponseDatas.add(assessmentData);
                                            }
                                            itemAdapter.notifyDataSetChanged();
                                            itemAdapter.setLoaded();
                                        }
                                    } catch (Exception e) {
                                        Log.e(TAG, "CompanyListResponse Check Error:" + e.toString());
                                    }
                                }

                                @Override
                                public void onFailure(Call<CompanyListResponse> call, Throwable t) {
                                    Log.e(TAG, "CompanyListResponse Error:" + t.toString());
                                }
                            });
                        }
                    }, 2000);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == FILTER_CODE) {
            filterRate = data.getStringExtra("Rate");
            filterSearchText = data.getStringExtra("SearchText");
            companyList();
        }
    }
}
