package com.example.tekno.arab_project.Model.Service.FeatureListSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeatureListResponseData {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("IconPath")
    @Expose
    private String iconPath;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Group")
    @Expose
    private String group;
    @SerializedName("ExtraPay")
    @Expose
    private String extraPay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getExtraPay() {
        return extraPay;
    }

    public void setExtraPay(String extraPay) {
        this.extraPay = extraPay;
    }
}
