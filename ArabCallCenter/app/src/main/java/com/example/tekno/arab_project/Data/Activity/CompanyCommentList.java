package com.example.tekno.arab_project.Data.Activity;

import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.ILoadMore;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.CompanyCommentList.ItemAdapter;
import com.example.tekno.arab_project.Model.CompanyCommentList.CompanyCommentListModel;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListAPI;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListModel;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListResponse;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListResponseData;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListRetrofitGET;
import com.example.tekno.arab_project.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyCommentList extends AppCompatActivity {
    private static final String TAG= CompanyCommentList.class.getName();
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private List<CompanyCommentListModel> companyCommentListModels;
    private List<AssessmentListResponseData> assessmentListResponseData;
    private LinearLayoutManager layoutManager;
    private ItemAdapter itemAdapter;
    private AssessmentListResponse assessmentListResponse;

    private Util util;
    private static String companyId;
    private Integer skip;
    private Integer take;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_comment_list);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title=(TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.company_comment_list));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            companyId = bundle.getString("companyId");
        }

        companyCommentListModels=new ArrayList<>();
        assessmentListResponseData=new ArrayList<>();
        skip= Constants.SKIP;
        take= Constants.TAKE;

        recyclerView=(RecyclerView) findViewById(R.id.commentListRcy);
        getCommentList();
    }

    private void getCommentList() {
        AssessmentListModel assesmentListModel = new AssessmentListModel(companyId, skip, Constants.TAKE);
        SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        String access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");
        AssessmentListAPI assessmentListAPI = AssessmentListRetrofitGET.getRetrofitAssessmentList().create(AssessmentListAPI.class);
        Call<AssessmentListResponse> call = assessmentListAPI.assessmentList(access, assesmentListModel);
        call.enqueue(new Callback<AssessmentListResponse>() {
            @Override
            public void onResponse(Call<AssessmentListResponse> call, Response<AssessmentListResponse> response) {
                assessmentListResponse = response.body();
                try{
                    if (assessmentListResponse != null) {
                        assessmentListResponseData = assessmentListResponse.getResponseData();
                        getData(companyCommentListModels,assessmentListResponseData);
                    }
                }catch (Exception e){
                    Log.e(TAG,"AssessmentListResponse Check Error:"+e.toString());
                }
            }

            @Override
            public void onFailure(Call<AssessmentListResponse> call, Throwable t) {
                Log.e(TAG,"AssessmentListResponse Error:"+t.toString());
            }
        });
    }

    private void getData(final List<CompanyCommentListModel>companyCommentListModels, List<AssessmentListResponseData> assessmentListResponseData) {

        for (AssessmentListResponseData assessmentData : assessmentListResponseData) {
            companyCommentListModels.add(new CompanyCommentListModel(util.getDateCurrent(), assessmentData.getRate(), assessmentData.getComment()));
        }
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        itemAdapter = new ItemAdapter(recyclerView, this, companyCommentListModels);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {
                if (companyCommentListModels.size()>=10) {
                    companyCommentListModels.add(null);
                    itemAdapter.notifyItemInserted(companyCommentListModels.size() - 1);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            companyCommentListModels.remove(companyCommentListModels.size() - 1);
                            itemAdapter.notifyItemRemoved(companyCommentListModels.size() - 1);
                            skip+=take;
                            AssessmentListModel assesmentListModel = new AssessmentListModel(companyId, skip, Constants.TAKE);
                            SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
                            String access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");
                            AssessmentListAPI assessmentListAPI = AssessmentListRetrofitGET.getRetrofitAssessmentList().create(AssessmentListAPI.class);
                            Call<AssessmentListResponse> call = assessmentListAPI.assessmentList(access, assesmentListModel);
                            call.enqueue(new Callback<AssessmentListResponse>() {
                                @Override
                                public void onResponse(Call<AssessmentListResponse> call, Response<AssessmentListResponse> response) {
                                    assessmentListResponse = response.body();

                                    try{
                                        if (assessmentListResponse != null) {

                                            for (AssessmentListResponseData assessmentData : assessmentListResponse.getResponseData()) {
                                                companyCommentListModels.add(new CompanyCommentListModel(util.getDateCurrent(), assessmentData.getRate(), assessmentData.getComment()));
                                            }
                                            itemAdapter.notifyDataSetChanged();
                                            itemAdapter.setLoaded();
                                        }

                                    }catch (Exception e){
                                        Log.e(TAG,"AssessmentListResponse Check Error:"+e.toString());
                                    }
                                }

                                @Override
                                public void onFailure(Call<AssessmentListResponse> call, Throwable t) {
                                    Log.e(TAG,"AssessmentListResponse Null Check Error:"+t.toString());
                                }
                            });
                        }
                    }, 2000);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
