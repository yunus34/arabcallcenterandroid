package com.example.tekno.arab_project.Model.Service.UserUpdatePassword;

import com.example.tekno.arab_project.Model.Service.UserUpdateSync.UserUpdateModel;
import com.example.tekno.arab_project.Model.Service.UserUpdateSync.UserUpdateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserUpdatePasswordAPI {
    @POST("ChangePassword")
    Call<UserUpdatePasswordResponse> userUpdatePassword(
            @Header("Authorization") String auth,
            @Body UserUpdatePasswordModel userUpdatePasswordModel);
}
