package com.example.tekno.arab_project.Data.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.tekno.arab_project.Data.Adapters.PagerAdapter;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.R;

public class TabLayoutActivity extends AppCompatActivity {

    private int LOCATION_PERMISSION_CODE = 1;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PagerAdapter adapter;
    private String emailText="Email";
    private String paswordText="Password";
    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablayout);
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            email=bundle.getString(emailText);
            password=bundle.getString(paswordText);
        }
        //checkPermission();


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new PagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        View loginTabView = getLayoutInflater().inflate(R.layout.custom_tab_login, null);
        tabLayout.getTabAt(0).setCustomView(loginTabView);
        View registerTabView = getLayoutInflater().inflate(R.layout.custom_tab_register, null);
        tabLayout.getTabAt(1).setCustomView(registerTabView);

        if(!Util.checkAndRequestPermissions(this)){
            return;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
     /*private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        }
    }
    private void requestPermission()
    {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==LOCATION_PERMISSION_CODE)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }*/
}
