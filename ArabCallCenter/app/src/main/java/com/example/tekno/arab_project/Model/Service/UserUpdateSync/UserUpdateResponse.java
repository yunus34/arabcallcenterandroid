package com.example.tekno.arab_project.Model.Service.UserUpdateSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserUpdateResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("responseName")
    @Expose
    private String responseName;
    @SerializedName("responseData")
    @Expose
    private UserUpdateResponseData responseData;
    @SerializedName("error")
    @Expose
    private Object error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public UserUpdateResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(UserUpdateResponseData responseData) {
        this.responseData = responseData;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}
