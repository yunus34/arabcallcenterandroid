package com.example.tekno.arab_project.Model.Service.AssessmentInsertSync;

public class AssessmentInsertModel {

    private String UserId;
    private String CompanyId;
    private String AssessmentTypeId;
    private int Rate;
    private String Comment;

    public AssessmentInsertModel(String userId, String companyId, String assessmentTypeId, int rate, String comment) {
        this.UserId = userId;
        this.CompanyId = companyId;
        this.AssessmentTypeId = assessmentTypeId;
        this.Rate = rate;
        this.Comment = comment;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getAssessmentTypeId() {
        return AssessmentTypeId;
    }

    public void setAssessmentTypeId(String assessmentTypeId) {
        AssessmentTypeId = assessmentTypeId;
    }

    public int getRate() {
        return Rate;
    }

    public void setRate(int rate) {
        Rate = rate;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }
}
