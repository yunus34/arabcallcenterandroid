package com.example.tekno.arab_project.Data.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListInformationResponse;
import com.example.tekno.arab_project.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = MapActivity.class.getName();

    private int LOCATION_PERMISSION_CODE = 100;
    private Toolbar toolbar;
    private MapFragment mapview;
    private GoogleMap gMap;
    private String location;
    private Util util;
    private CompanyListInformationResponse companyListInformationResponse;
    private String title;
    private String[] latlng;
    private String[] companyLatLng;
    private int checkCompany;
    private double lat;
    private double lng;
    private boolean permission = false;
    private boolean gps_enabled;
    private boolean network_enabled;

    public static Intent newIntent(Activity activity, String[] latLng, int checkCompany, String title) {
        Intent intent = new Intent(activity, MapActivity.class);
        intent.putExtra("LatLng", latLng);
        intent.putExtra("CompanyDetailchack", checkCompany);
        intent.putExtra("CompanyTitle", title);
        return intent;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.company_map));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            companyListInformationResponse = (CompanyListInformationResponse) bundle.getSerializable("companyInMap");
            companyLatLng = bundle.getStringArray("LatLng");
            checkCompany = bundle.getInt("CompanyDetailchack");
            title = bundle.getString("CompanyTitle");

        }
        if (companyListInformationResponse != null) {
            latlng = companyListInformationResponse.getLocationInfo().split(";");
            lat = Double.valueOf(latlng[0]);
            lng = Double.valueOf(latlng[1]);
            title = companyListInformationResponse.getTitle();
        } else {
            lat = Double.valueOf(companyLatLng[0]);
            lng = Double.valueOf(companyLatLng[1]);
        }

        mapview = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapview.getMapAsync(this);
        checkPermissionLocation();

        if(!getChecklocation()){
            String msg=getResources().getString(R.string.dialog_location_message);
            // true Diaolg show location page from MapActivity
            util.getDialogInformation(this,msg,true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (permission) {
            gMap = googleMap;
            gMap.setMyLocationEnabled(true);
            try {
                BitmapDescriptor icon = Util.bitmapDescriptorFromVector(this, R.drawable.ic_location);
                Marker marker = gMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lng))
                        .title(title)
                        .icon(icon)
                );
                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.0f));
            } catch (Exception e) {
                Log.e(TAG, "Map lat lng error:" + e.toString());
            }
        }
        else{ String msg=getResources().getString(R.string.dialog_location_permission_message);
            // false Diaolg show location information on MapActivity
            util.getDialogInformation(this,msg,false);}
    }

    private void checkPermissionLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        }else{
            permission=true;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permission = true;
            } else {
                permission = false;
            }
        }
    }
    public boolean getChecklocation(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        }catch (Exception ex){
            Log.e(TAG, "GPS enabled:" + ex.toString());
        }
        try{
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch (Exception ex){
            Log.e(TAG, "NETWORK enabled:" + ex.toString());
        }
        if(!gps_enabled || !network_enabled){
            return false;
        }
        return true;
    }
}
