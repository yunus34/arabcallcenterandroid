package com.example.tekno.arab_project.Data.Database;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.UUID;


public class BaseEntity implements Serializable {
    public static final String ID = "Id";
    public static final String CREATE_DATE = "CreateDate";
    public static final String UPDATE_DATE = "UpdateDate";
    public static final String IS_DELETED = "IsDeleted";


    @DatabaseField(columnName = ID, id = true)
    private UUID Id;
    @DatabaseField(columnName = CREATE_DATE)
    private String createDate;
    @DatabaseField(columnName = UPDATE_DATE)
    private String updateDate;
    @DatabaseField(columnName = IS_DELETED)
    private Boolean isDeleted;

    public UUID getId() {
        return Id;
    }

    public void setId(UUID Id) {
        this.Id = Id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}
