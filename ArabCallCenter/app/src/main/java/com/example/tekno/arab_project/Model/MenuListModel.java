package com.example.tekno.arab_project.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class MenuListModel implements Parcelable {
    private String name;
    private String image;
    private Integer sequence;

    public MenuListModel(String name, String image, Integer sequence) {
        this.name = name;
        this.image = image;
        this.sequence = sequence;
    }

    protected MenuListModel(Parcel in) {
        name = in.readString();
        image = in.readString();
        if (in.readByte() == 0) {
            sequence = null;
        } else {
            sequence = in.readInt();
        }
    }

    public static final Creator<MenuListModel> CREATOR = new Creator<MenuListModel>() {
        @Override
        public MenuListModel createFromParcel(Parcel in) {
            return new MenuListModel(in);
        }

        @Override
        public MenuListModel[] newArray(int size) {
            return new MenuListModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(image);
        if (sequence == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(sequence);
        }
    }
}