package com.example.tekno.arab_project.Model.Service.CompanyListSync;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CompanyListAPI {
    @POST("List")
    Call<CompanyListResponse> companyList(
            @Header("Authorization") String auth,
            @Body CompanyListModel companyCommentListModel
            );
}
