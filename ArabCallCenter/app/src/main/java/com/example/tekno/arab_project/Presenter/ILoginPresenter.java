package com.example.tekno.arab_project.Presenter;

import android.app.Activity;

public interface ILoginPresenter {
    void performLogin(Activity activity, String userEmail, String password, String username, boolean remember_me, boolean socialSignInValid);
}
