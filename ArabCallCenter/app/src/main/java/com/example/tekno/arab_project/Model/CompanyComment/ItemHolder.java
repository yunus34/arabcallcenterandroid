package com.example.tekno.arab_project.Model.CompanyComment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.R;

public class ItemHolder extends RecyclerView.ViewHolder {

    public TextView commentDate;
    public RatingBar ratingBar;
    public TextView commentText;
    public Context context;
    private Util util;

    public ItemHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        this.util = new Util(context);

        commentDate = (TextView) itemView.findViewById(R.id.commentDate);
        commentDate.setTypeface(util.getTypeface());
        ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
        commentText = (TextView) itemView.findViewById(R.id.commentText);
        commentText.setTypeface(util.getTypeface());
    }
}
