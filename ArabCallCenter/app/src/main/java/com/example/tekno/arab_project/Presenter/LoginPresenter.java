package com.example.tekno.arab_project.Presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Database.DB_Helper;
import com.example.tekno.arab_project.Data.Database.Querys.GeneralManager;
import com.example.tekno.arab_project.Data.Database.MenuList;
import com.example.tekno.arab_project.Data.Database.Parameters;
import com.example.tekno.arab_project.Data.Database.Users;
import com.example.tekno.arab_project.Data.Enum.RegisterDataValidation;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.Service.LoginSync.LoginAPI;
import com.example.tekno.arab_project.Model.Service.LoginSync.LoginModel;
import com.example.tekno.arab_project.Model.Service.LoginSync.LoginResponse;
import com.example.tekno.arab_project.Model.Service.LoginSync.LoginRetrofitGET;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterAPI;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterModel;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterResponse;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterResponseError;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterRetrofitGET;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenAPI;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenModel;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenResponse;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenRetrofitGET;
import com.example.tekno.arab_project.R;
import com.example.tekno.arab_project.View.LoginView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements ILoginPresenter {
    private String TAG = LoginPresenter.class.getName();
    private LoginView loginView;
    private TokenResponse tokenResponse;
    private SharedPreferences sharedPreferencesToken, sharedPrefRememberMe, sharedPrefRememberMeRemove, sharedPreferencesUserId, sharedPreferencesUserEmail, sharedPreferencesUserInformation;
    private SharedPreferences.Editor editorToken, editorRememberMe, editorRememberMeRemove, editorUserId, editorUserEmail, editorUserInformation;
    private ProgressDialog progressDialog;
    private String access;
    private LoginResponse response;
    private RegisterResponse responseRegister;
    private RegisterResponseError registerResponseError;
    private GeneralManager generalManager;

    private DB_Helper db_helper;
    private Parameters parameters;
    private MenuList menuList;
    private Users users;

    private Activity activity;
    private String userEmail;
    private String username;
    private String password;
    private boolean remember_me;
    private boolean socialSignInValid;

    private List<MenuList> menuLists;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
        response = new LoginResponse();
    }

    @Override
    public void performLogin(final Activity activity, final String userEmail, final String password, final String username, boolean remember_me, boolean socialSignInValid) {
        this.activity = activity;
        this.userEmail = userEmail;
        this.password = password;
        this.username = username;
        this.socialSignInValid = socialSignInValid;
        this.remember_me=remember_me;

        db_helper = new DB_Helper(activity);
        generalManager = new GeneralManager(db_helper);
        parameters = new Parameters();
        users = new Users();
        menuList = new MenuList();
        menuLists = new ArrayList<>();
        try {
            progressDialog = new ProgressDialog(activity);
            progressDialog.show();
            Util.setProgressDialog(progressDialog);

            if (socialSignInValid) {
                socialSignIn();
            } else {
                TokenModel tokenModel = new TokenModel(userEmail, password, Constants.GRANT_TYPE);
                boolean isLoginSuccess = tokenModel.isEmailValidData();

                if (isLoginSuccess) {

                    TokenAPI tokenAPI = TokenRetrofitGET.getRetrofitToken().create(TokenAPI.class);
                    Call<TokenResponse> call = tokenAPI.token(tokenModel.getUserEmail(), tokenModel.getPassword(), tokenModel.getGrant_type());
                    call.enqueue(new Callback<TokenResponse>() {
                        @Override
                        public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                            tokenResponse = response.body();
                            if (tokenResponse != null) {
                                sharedPreferencesToken = activity.getSharedPreferences(Constants.ACCESS_TOKEN, 0);
                                editorToken = sharedPreferencesToken.edit();
                                editorToken.putString("access_token", tokenResponse.getAccessToken());
                                editorToken.putString("refresh_token", tokenResponse.getRefreshToken());
                                editorToken.commit();

                                access = Constants.TOKEN_TYPE + " " + tokenResponse.getAccessToken();

                                try {
                                    if (generalManager.retrieveParameters().size() <= 0) {
                                        parameters.setId(UUID.randomUUID());
                                        parameters.setCreateDate(Util.getCurrentDate());
                                        parameters.setDeleted(false);
                                        parameters.setUpdateDate(Util.getCurrentDate());
                                        parameters.setName(Util.getCurrentMinDate());
                                        parameters.setDescription("first");
                                        parameters.setGroupName("MenuController");

                                        db_helper.createOrUpdate(parameters);
                                    }
                                    login();

                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    Log.e(TAG, "SQL Create Parameters Error:" + e);
                                    Util.dismisProgressDialog(progressDialog);
                                }
                            } else {
                                Toasty.error(activity, activity.getResources().getText(R.string.validate_login_email_password), Toast.LENGTH_LONG).show();
                                Util.dismisProgressDialog(progressDialog);
                            }
                        }

                        @Override
                        public void onFailure(Call<TokenResponse> call, Throwable t) {
                            Log.e(TAG, "TokenResponse Error:" + t.toString());
                            loginView.loginError(t);
                            Util.dismisProgressDialog(progressDialog);
                        }
                    });
                } else {
                    loginView.loginError();
                    Util.dismisProgressDialog(progressDialog);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Login Error:" + e.toString());
            Util.dismisProgressDialog(progressDialog);
        }
    }

    public void login() {

        LoginModel loginModel = new LoginModel(userEmail, password);
        LoginAPI loginAPI = LoginRetrofitGET.getRetrofitLogin().create(LoginAPI.class);
        Observable<LoginResponse> call = loginAPI.login(access, loginModel);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<LoginResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        if (loginResponse != null) {
                            response = loginResponse;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Login Error:" + e.toString());
                        loginView.loginError(e);
                        Util.dismisProgressDialog(progressDialog);
                    }

                    @Override
                    public void onComplete() {
                        if (response.getSuccess()) {
                            sharedPreferencesUserInformation = activity.getSharedPreferences(Constants.USER_INFORMATIONS, 0);
                            editorUserInformation = sharedPreferencesUserInformation.edit();
                            editorUserInformation.putString("UserId", String.valueOf(response.getLoginResponseUserData().getUserId()));
                            editorUserInformation.putString("Name", response.getLoginResponseUserData().getUserName());
                            editorUserInformation.putString("Surname", response.getLoginResponseUserData().getUserSurname());
                            editorUserInformation.putString("Email", response.getLoginResponseUserData().getUserEmail());
                            editorUserInformation.putString("GsmNo", response.getLoginResponseUserData().getGsmNo());
                            editorUserInformation.putString("Password", password);
                            editorUserInformation.commit();

                            if (remember_me) {
                                sharedPrefRememberMe = activity.getSharedPreferences(Constants.USER_REMEMBER_ME, 0);
                                editorRememberMe = sharedPrefRememberMe.edit();
                                editorRememberMe.putString("Email", username);
                                editorRememberMe.putString("Password", password);
                                editorRememberMe.commit();
                            } else {
                                sharedPrefRememberMeRemove = activity.getSharedPreferences(Constants.USER_REMEMBER_ME, 0);
                                editorRememberMeRemove = sharedPrefRememberMeRemove.edit();
                                editorRememberMeRemove.remove("Email");
                                editorRememberMeRemove.remove("Password");
                                editorRememberMeRemove.commit();
                            }

                            loginView.loginSuccess();
                            Log.e(TAG, "Login Susscess:");
                            Util.dismisProgressDialog(progressDialog);
                        } else {
                            Toasty.error(activity, activity.getResources().getText(R.string.validate_login_email_password), Toast.LENGTH_LONG).show();
                            Util.dismisProgressDialog(progressDialog);
                        }
                    }
                });
    }

    private void socialSignIn() {
       /* RegisterModel registerModel = null;
        if (socialChoose == RegisterDataValidation.GoogleSignIn.getValid()) {
            registerModel = new RegisterModel(userEmail, password, "", username);
        } else if (socialChoose == RegisterDataValidation.FacebookSignIn.getValid()) {
            registerModel = new RegisterModel(userMail, userPassword, gsmNo, "");
        }*/
        try {
            RegisterModel registerModel = new RegisterModel(userEmail, password, "", username);
            RegisterAPI registerAPI = RegisterRetrofitGET.getRetrofit().create(RegisterAPI.class);
            Observable<RegisterResponse> call = registerAPI.register(Constants.ApplicationSignature, registerModel);
            call.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new io.reactivex.Observer<RegisterResponse>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onNext(RegisterResponse registerResponse) {
                            responseRegister = registerResponse;

                            if (registerResponse != null && registerResponse.getSuccess()) {
                                users.setId(registerResponse.getRegisterResponseUserData().getId());
                                users.setDeleted(registerResponse.getRegisterResponseUserData().getDeleted());
                                users.setCreateDate(registerResponse.getRegisterResponseUserData().getCreateDate());
                                users.setUpdateDate(registerResponse.getRegisterResponseUserData().getUpdateDate());

                                users.setUserEmail(registerResponse.getRegisterResponseUserData().getEmail());
                                users.setUserName(registerResponse.getRegisterResponseUserData().getName());
                                users.setUserSurName(registerResponse.getRegisterResponseUserData().getSurname());
                                users.setUserPassword(registerResponse.getRegisterResponseUserData().getPassword());
                                users.setUserPhone(registerResponse.getRegisterResponseUserData().getGsmNo());
                                users.setUserIsBanned(registerResponse.getRegisterResponseUserData().getBanned());
                                try {
                                    db_helper.getDao(Users.class).create(users);
                                } catch (SQLException e) {
                                    Log.d(TAG, e.toString());
                                    loginView.registerDatabaseError(e);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "RegisterResponse Error:" + e.toString());
                            Toasty.error(activity, activity.getResources().getText(R.string.validate_register), Toast.LENGTH_LONG).show();
                            Util.dismisProgressDialog(progressDialog);
                        }

                        @Override
                        public void onComplete() {
                            if (responseRegister != null && responseRegister.getSuccess()) {
                                loginView.registerSuccess();
                            } else {
                                registerResponseError = responseRegister.getError();
                                if (registerResponseError.getErrorCode() == RegisterDataValidation.UserRequired.getValid())
                                  loginView.loginError(RegisterDataValidation.UserRequired.getValid());                                Util.dismisProgressDialog(progressDialog);
                            }
                        }
                    });
        } catch (Exception e) {
            Log.e(TAG, "Register Error:" + e.toString());
        }
    }

}