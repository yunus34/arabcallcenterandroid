package com.example.tekno.arab_project.Model.Service.TokenSync;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TokenRetrofitGET {
    private static Retrofit retrofit = null;
    private static String BASE_URL_TOKEN = Constants.Token_URL;

    public static Retrofit getRetrofitToken() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_TOKEN)
                    .client(Util.getTimeOut())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }
        return retrofit;
    }
}
