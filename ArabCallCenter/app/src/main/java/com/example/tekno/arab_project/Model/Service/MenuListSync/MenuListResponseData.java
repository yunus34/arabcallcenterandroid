package com.example.tekno.arab_project.Model.Service.MenuListSync;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MenuListResponseData implements Parcelable {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("IconPath")
    @Expose
    private String iconPath;
    @SerializedName("JsonInfo")
    @Expose
    private Object jsonInfo;
    @SerializedName("IsDeleted")
    @Expose
    private Object isDeleted;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CreateUser")
    @Expose
    private Object createUser;
    @SerializedName("UpdateDate")
    @Expose
    private Object updateDate;
    @SerializedName("UpdateUser")
    @Expose
    private Object updateUser;
    @SerializedName("Sequence")
    @Expose
    private Integer sequence;
    @SerializedName("Company")
    @Expose
    private List<Object> company = null;

    protected MenuListResponseData(Parcel in) {
        id = in.readString();
        name = in.readString();
        iconPath = in.readString();
        createDate = in.readString();
        if (in.readByte() == 0) {
            sequence = null;
        } else {
            sequence = in.readInt();
        }
    }

    public static final Creator<MenuListResponseData> CREATOR = new Creator<MenuListResponseData>() {
        @Override
        public MenuListResponseData createFromParcel(Parcel in) {
            return new MenuListResponseData(in);
        }

        @Override
        public MenuListResponseData[] newArray(int size) {
            return new MenuListResponseData[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public Object getJsonInfo() {
        return jsonInfo;
    }

    public void setJsonInfo(Object jsonInfo) {
        this.jsonInfo = jsonInfo;
    }

    public Object getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public List<Object> getCompany() {
        return company;
    }

    public void setCompany(List<Object> company) {
        this.company = company;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(iconPath);
        dest.writeString(createDate);
        if (sequence == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(sequence);
        }
    }
}
