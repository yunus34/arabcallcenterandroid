package com.example.tekno.arab_project.Model.Service.CompanyDetailSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyDetailInformationResponse {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Phone1")
    @Expose
    private String phone1;
    @SerializedName("Phone2")
    @Expose
    private String phone2;
    @SerializedName("MenuInfoId")
    @Expose
    private String menuInfoId;
    @SerializedName("WebAddress")
    @Expose
    private String webAddress;
    @SerializedName("Rating")
    @Expose
    private Double rating;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("LocationInfo")
    @Expose
    private String locationInfo;
    @SerializedName("JsonInfoData")
    @Expose
    private String jsonInfoData;
    @SerializedName("Fax")
    @Expose
    private String fax;
    @SerializedName("AuthPersonName")
    @Expose
    private String authPersonName;
    @SerializedName("AuthPersonSurname")
    @Expose
    private String authPersonSurname;
    @SerializedName("AuthPersonPhone")
    @Expose
    private String authPersonPhone;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("TaxNumber")
    @Expose
    private Object taxNumber;
    @SerializedName("TaxOfficeId")
    @Expose
    private Object taxOfficeId;
    @SerializedName("MersisNo")
    @Expose
    private Object mersisNo;
    @SerializedName("SectorId")
    @Expose
    private Object sectorId;
    @SerializedName("IsDeleted")
    @Expose
    private Object isDeleted;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CreateUser")
    @Expose
    private String createUser;
    @SerializedName("UpdateDate")
    @Expose
    private Object updateDate;
    @SerializedName("UpdateUser")
    @Expose
    private Object updateUser;
    @SerializedName("LogoPath")
    @Expose
    private String logoPath;
    @SerializedName("Complaint")
    @Expose
    private List<Object> complaint = null;
    @SerializedName("Assessment")
    @Expose
    private List<Object> assessment = null;
    @SerializedName("Sector")
    @Expose
    private Object sector;
    @SerializedName("MenuInfo")
    @Expose
    private Object menuInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getMenuInfoId() {
        return menuInfoId;
    }

    public void setMenuInfoId(String menuInfoId) {
        this.menuInfoId = menuInfoId;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(String locationInfo) {
        this.locationInfo = locationInfo;
    }

    public String getJsonInfoData() {
        return jsonInfoData;
    }

    public void setJsonInfoData(String jsonInfoData) {
        this.jsonInfoData = jsonInfoData;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAuthPersonName() {
        return authPersonName;
    }

    public void setAuthPersonName(String authPersonName) {
        this.authPersonName = authPersonName;
    }

    public String getAuthPersonSurname() {
        return authPersonSurname;
    }

    public void setAuthPersonSurname(String authPersonSurname) {
        this.authPersonSurname = authPersonSurname;
    }

    public String getAuthPersonPhone() {
        return authPersonPhone;
    }

    public void setAuthPersonPhone(String authPersonPhone) {
        this.authPersonPhone = authPersonPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(Object taxNumber) {
        this.taxNumber = taxNumber;
    }

    public Object getTaxOfficeId() {
        return taxOfficeId;
    }

    public void setTaxOfficeId(Object taxOfficeId) {
        this.taxOfficeId = taxOfficeId;
    }

    public Object getMersisNo() {
        return mersisNo;
    }

    public void setMersisNo(Object mersisNo) {
        this.mersisNo = mersisNo;
    }

    public Object getSectorId() {
        return sectorId;
    }

    public void setSectorId(Object sectorId) {
        this.sectorId = sectorId;
    }

    public Object getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public List<Object> getComplaint() {
        return complaint;
    }

    public void setComplaint(List<Object> complaint) {
        this.complaint = complaint;
    }

    public List<Object> getAssessment() {
        return assessment;
    }

    public void setAssessment(List<Object> assessment) {
        this.assessment = assessment;
    }

    public Object getSector() {
        return sector;
    }

    public void setSector(Object sector) {
        this.sector = sector;
    }

    public Object getMenuInfo() {
        return menuInfo;
    }

    public void setMenuInfo(Object menuInfo) {
        this.menuInfo = menuInfo;
    }
}
