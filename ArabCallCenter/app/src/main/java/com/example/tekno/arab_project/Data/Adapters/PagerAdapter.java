package com.example.tekno.arab_project.Data.Adapters;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.tekno.arab_project.Data.Fragment.LoginFragment;
import com.example.tekno.arab_project.Data.Fragment.RegisterFragment;

public class PagerAdapter extends FragmentPagerAdapter {
    private Context mContext;

    public PagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new LoginFragment();
        } else {
            return new RegisterFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        /*switch (position) {
            case 0:
                return mContext.getString(R.string.login);

            case 1:
                return mContext.getString(R.string.register);
            default:
                return null;
        }*/
        return null;
    }
}
