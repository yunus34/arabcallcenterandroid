package com.example.tekno.arab_project.Data.Database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Parameters")
public class Parameters extends BaseEntity {

    public static final String NAME = "Name";
    public static final String DESCRIPTION = "Description";
    public static final String GROUP_NAME = "GroupName";

    @DatabaseField(columnName = NAME)
    private String name;

    @DatabaseField(columnName = DESCRIPTION)
    private String description;

    @DatabaseField(columnName = GROUP_NAME)
    private String groupName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
