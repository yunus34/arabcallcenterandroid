package com.example.tekno.arab_project.Data.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.R;

public class SplashActivity extends Activity {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    sharedPreferences = getSharedPreferences(Constants.USER_INFORMATIONS, 0);
                    if (!sharedPreferences.getString("Email", "").equals("") &&
                            !sharedPreferences.getString("Password", "").equals("")){
                        Intent intentMain = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(intentMain);
                    }else{
                        Intent intentTabLayout = new Intent(SplashActivity.this, TabLayoutActivity.class);
                        startActivity(intentTabLayout);

                    }



                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
