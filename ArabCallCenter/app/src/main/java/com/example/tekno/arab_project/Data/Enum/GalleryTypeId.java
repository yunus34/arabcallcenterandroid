package com.example.tekno.arab_project.Data.Enum;

public enum GalleryTypeId {
    Image(7),
    Video(8);

    private int value;

    GalleryTypeId(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
