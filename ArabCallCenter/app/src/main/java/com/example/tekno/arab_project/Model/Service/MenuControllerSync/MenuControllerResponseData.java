package com.example.tekno.arab_project.Model.Service.MenuControllerSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuControllerResponseData {
    @SerializedName("updateRequired")
    @Expose
    private Boolean updateRequired;
    @SerializedName("updateDate")
    @Expose
    private String updateDate;

    public Boolean getUpdateRequired() {
        return updateRequired;
    }

    public void setUpdateRequired(Boolean updateRequired) {
        this.updateRequired = updateRequired;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
