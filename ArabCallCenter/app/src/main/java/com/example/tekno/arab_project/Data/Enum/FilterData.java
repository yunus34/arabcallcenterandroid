package com.example.tekno.arab_project.Data.Enum;

public enum FilterData {
    Zero(0),
    Five(5),
    Six(6),
    Eight(8),
    Nine(9);

    private int value;

    FilterData(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
