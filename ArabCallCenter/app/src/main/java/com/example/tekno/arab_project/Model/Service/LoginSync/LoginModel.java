package com.example.tekno.arab_project.Model.Service.LoginSync;

public class LoginModel {
    private String Email;
    private String Password;

    public LoginModel(String userEmail, String userPassword) {
        this.Email = userEmail;
        this.Password = userPassword;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
