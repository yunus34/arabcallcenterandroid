package com.example.tekno.arab_project.Model.Service.AssessmentListSync;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AssessmentListAPI {
    @POST("List")
    Call<AssessmentListResponse> assessmentList(
            @Header("Authorization") String auth,
            @Body AssessmentListModel assessmentListModel
    );
}

