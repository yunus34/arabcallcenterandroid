package com.example.tekno.arab_project.Model.CompanyList;

public class CompanyListModel {
    private int[] companyImages;
    private Double companyRate;
    private String companyName;
    private String companyPhone;

    public CompanyListModel(int[] companyImages, Double companyRate, String companyName, String companyPhone) {
        this.companyImages = companyImages;
        this.companyRate = companyRate;
        this.companyName = companyName;
        this.companyPhone = companyPhone;
    }


    public int[] getCompanyImages() {
        return companyImages;
    }

    public void setCompanyImages(int[] companyImages) {
        this.companyImages = companyImages;
    }

    public Double getCompanyRate() {
        return companyRate;
    }

    public void setCompanyRate(Double companyRate) {
        this.companyRate = companyRate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }
}
