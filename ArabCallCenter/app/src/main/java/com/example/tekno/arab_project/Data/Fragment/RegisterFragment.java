package com.example.tekno.arab_project.Data.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Enum.RegisterDataValidation;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Presenter.RegisterPresenter;
import com.example.tekno.arab_project.R;
import com.example.tekno.arab_project.View.RegisterView;

import java.sql.SQLException;

import es.dmoral.toasty.Toasty;


public class RegisterFragment extends Fragment implements RegisterView {
    private static final String TAG=RegisterFragment.class.getName();
    private EditText email;
    private EditText gsmNo;
    private EditText password;
    private TextView register;
    private RegisterPresenter registerPresenter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_register, container, false);
        email = (EditText)view.findViewById(R.id.email);
        password = (EditText)view.findViewById(R.id.password);
        gsmNo = (EditText)view.findViewById(R.id.gsmNoText);
        register=(TextView)view.findViewById(R.id.register);

        registerPresenter=new RegisterPresenter(this);
        Typeface newTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        email.setTypeface(newTypeface);
        gsmNo.setTypeface(newTypeface);
        password.setTypeface(newTypeface);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Util.isEmpty(email))
                {
                    email.requestFocus();
                    email.setError(getResources().getText(R.string.validate_email_empty));
                }
                else if(Util.isEmpty(password))
                {
                    password.requestFocus();
                    password.setError(getResources().getText(R.string.validate_pw_empty));
                }
                /*else if(Util.isEmpty(gsmNo))
                {
                    gsmNo.requestFocus();
                    gsmNo.setError(getResources().getText(R.string.validate_phone_empty));
                }*/
                else{
                    registerPresenter.performRegister(getActivity(),email.getText().toString().trim(),
                            password.getText().toString().trim(),gsmNo.getText().toString().trim());
                }
            }
        });

        return view;
    }

    @Override
    public void registerSuccess() {
        Toasty.success(getActivity(),getResources().getText(R.string.validate_register_success), Toast.LENGTH_SHORT).show();
          }

    @Override
    public void registerError(Integer valid) {
        if(valid== RegisterDataValidation.Email.getValid())
        {
            Toasty.error(getActivity(),getResources().getText(R.string.validate_register_email), Toast.LENGTH_SHORT).show();
        }
        else if(valid== RegisterDataValidation.Password.getValid())
        {
            Toasty.error(getActivity(),getResources().getText(R.string.validate_register_password), Toast.LENGTH_SHORT).show();
        }
        else if(valid== RegisterDataValidation.Phone.getValid())
        {
            Toasty.error(getActivity(),getResources().getText(R.string.validate_register_phone), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void registerDatabaseError(SQLException e) {

    }
}
