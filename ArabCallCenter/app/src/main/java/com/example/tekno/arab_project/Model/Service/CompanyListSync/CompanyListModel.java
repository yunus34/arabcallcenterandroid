package com.example.tekno.arab_project.Model.Service.CompanyListSync;

import java.util.UUID;

public class CompanyListModel {
    private String ModelId;
    private Integer Skip;
    private Integer Take;
    private String Rate;
    private String SearchText;

    public CompanyListModel(String modelId, Integer skip, Integer take, String rate, String searchText) {
        this.ModelId = modelId;
        this.Skip = skip;
        this.Take = take;
        this.Rate = rate;
        this.SearchText = searchText;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public Integer getSkip() {
        return Skip;
    }

    public void setSkip(Integer skip) {
        Skip = skip;
    }

    public Integer getTake() {
        return Take;
    }

    public void setTake(Integer take) {
        Take = take;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getSearchText() {
        return SearchText;
    }

    public void setSearchText(String searchText) {
        SearchText = searchText;
    }
}
