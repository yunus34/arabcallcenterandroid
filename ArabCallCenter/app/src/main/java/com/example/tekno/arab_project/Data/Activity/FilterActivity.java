package com.example.tekno.arab_project.Data.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.R;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private Button filterApply;
    private TextView searchText;
    private EditText search;
    private TextView evaluationText;
    private TextView locationText;
    private TextView distanceText;
    private TextView score1;
    private TextView score2;
    private TextView score3;
    private TextView score4;
    private TextView score5;
    private SeekBar seekbar_distance;

    private Util util;
    private String rate = "1";
    private String searchTxt;

    public static Intent newInstance(Activity activity, String rate, String searchText) {
        Intent intent = new Intent(activity, FilterActivity.class);
        intent.putExtra("Rate", rate);
        intent.putExtra("SearchText", searchText);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.filter_toolbar));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            rate = bundle.getString("Rate", "1");
            searchTxt = bundle.getString("SearchText", "");
        }

        searchText = (TextView) findViewById(R.id.searchText);
        searchText.setTypeface(util.getTypefaceBold());
        search = (EditText) findViewById(R.id.search);
        search.setTypeface(util.getTypeface());
        evaluationText = (TextView) findViewById(R.id.evaluationText);
        evaluationText.setTypeface(util.getTypefaceBold());
        locationText = (TextView) findViewById(R.id.locationText);
        locationText.setTypeface(util.getTypeface());
        distanceText = (TextView) findViewById(R.id.distanceText);
        distanceText.setTypeface(util.getTypeface());
        score1 = (TextView) findViewById(R.id.score1);
        score1.setTypeface(util.getTypeface());
        score2 = (TextView) findViewById(R.id.score2);
        score2.setTypeface(util.getTypeface());
        score3 = (TextView) findViewById(R.id.score3);
        score3.setTypeface(util.getTypeface());
        score4 = (TextView) findViewById(R.id.score4);
        score4.setTypeface(util.getTypeface());
        score5 = (TextView) findViewById(R.id.score5);
        score5.setTypeface(util.getTypeface());
        seekbar_distance = (SeekBar) findViewById(R.id.seekbar_distance);
        filterApply = (Button) findViewById(R.id.filterApply);
        filterApply.setTypeface(util.getTypeface());

        filterApply.setOnClickListener(this);
        score1.setOnClickListener(this);
        score2.setOnClickListener(this);
        score3.setOnClickListener(this);
        score4.setOnClickListener(this);
        score5.setOnClickListener(this);
        if(!searchTxt.equals("")){
            search.setText(searchTxt);
        }
        /*seekbar_distance.setMax(100);
        seekbar_distance.setProgress(20);
        seekbar_distance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                distanceText.setText(Double.valueOf(seekBar.getProgress()/5)+" km");
            }
        });*/

        if(rate.equals("1")){
            score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_zero));
            score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_five));
            score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
            score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
            score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
        }
        if(rate.equals("2")){
            score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_five));
            score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
            score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
            score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
        }
        if(rate.equals("3")){
            score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
            score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
            score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
        }
        if(rate.equals("4")){
            score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
            score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
        }
        if(rate.equals("5")){
            score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
            score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.score1:
                rate = "1";
                score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_zero));
                score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_five));
                score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
                score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
                score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
                break;

            case R.id.score2:
                rate = "2";
                score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_five));
                score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
                score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
                score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
                break;

            case R.id.score3:
                rate = "3";
                score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
                score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
                score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
                break;

            case R.id.score4:
                rate = "4";
                score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
                score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
                break;

            case R.id.score5:
                rate = "5";
                score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_general));
                break;

            case R.id.filterApply:
                Intent intent = new Intent();
                intent.putExtra("Rate", rate);
                intent.putExtra("SearchText", search.getText().toString().trim());
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.clear_filter:
                search.getText().clear();
                rate = "1";
                score1.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_zero));
                score2.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_five));
                score3.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_six));
                score4.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_eight));
                score5.setBackground(getResources().getDrawable(R.drawable.bg_score_filter_nine));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
