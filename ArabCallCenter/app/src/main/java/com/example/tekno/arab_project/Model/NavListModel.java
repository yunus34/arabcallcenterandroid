package com.example.tekno.arab_project.Model;

public class NavListModel {
    private int text;
    private int image;


    public NavListModel(int text, int image) {
        this.text = text;
        this.image = image;

    }
    public int getText() {
        return text;
    }

    public void setText(int text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
