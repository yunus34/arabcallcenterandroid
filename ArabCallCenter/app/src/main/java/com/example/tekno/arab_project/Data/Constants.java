package com.example.tekno.arab_project.Data;

public class Constants {
    public static String Token_URL="http://apiservice.arabcallcenter.com/api/";
    public static String Login_URL="http://apiservice.arabcallcenter.com/api/User/";
    public static String Register_URL="http://apiservice.arabcallcenter.com/api/User/";
    public static String Menu_Controller_URL="http://apiservice.arabcallcenter.com/api/Menu/";
    public static String Menu_List_URL="http://apiservice.arabcallcenter.com/api/Menu/";
    public static String Company_List_URL="http://apiservice.arabcallcenter.com/api/Company/";
    public static String Company_Detail_URL="http://apiservice.arabcallcenter.com/api/Company/";
    public static String Assessment_Insert_URL="http://apiservice.arabcallcenter.com/api/Assessment/";
    public static String Assessment_List_URL="http://apiservice.arabcallcenter.com/api/Assessment/";
    public static String User_Update_URL="http://apiservice.arabcallcenter.com/api/User/";
    public static String User_Update_Password_URL="http://apiservice.arabcallcenter.com/api/User/";
    public static String Feature_List_URL="http://apiservice.arabcallcenter.com/api/Feature/";

    public static String ApplicationSignature="codingFromCodedBrains";

    //Prefences
    public static String ACCESS_TOKEN="ACCESS_TOKEN";
    public static String USER_INFORMATIONS="USER_INFORMATIONS";
    public static String USER_REMEMBER_ME="USER_REMEMBER_ME";
    public static String GRANT_TYPE="password";
    public static String GRANT_TYPE_REFRESH_TOKEN="refresh_token";
    public static String TOKEN_TYPE="bearer";
    public static String ASSESSMENT_TYPE_ID="15";
    public static Integer SKIP=0;
    public static Integer TAKE=10;

    //YOUTUBE VIDEO KEY
    public static  String YOUTUBE_API_KEY="AIzaSyBWMdJn88BqZ3G7zECleSKe7lPR-8Xv5nA";
    //goole sign-in api
    public static  String GOOGLE_SIGN_IN_API_KEY="113549856408-lu6moskd5nf4dfmqeprp6tsl0icmsm45.apps.googleusercontent.com";

    //Google and Facebook password
    public static String googlePassword="google159753";
    public static String facebookPassword="facebook159753";
}