package com.example.tekno.arab_project.Model.Service.TokenSync;

import android.util.Patterns;

import com.example.tekno.arab_project.Data.Util;

public class TokenModel {
    public String userEmail;
    public String password;
    public String grant_type;

    public TokenModel(String userEmail, String password, String grant_type) {
        this.userEmail = userEmail;
        this.password = password;
        this.grant_type = grant_type;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public boolean isEmailValidData() {
        return Patterns.EMAIL_ADDRESS.matcher(getUserEmail()).matches();
    }
    }
