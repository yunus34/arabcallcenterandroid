package com.example.tekno.arab_project.Data.Enum;

public enum ItemPositionEnum {
    Vip(0),
    Hospital(1);

    private int value;

    ItemPositionEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
