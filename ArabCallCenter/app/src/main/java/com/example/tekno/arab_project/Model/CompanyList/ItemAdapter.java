package com.example.tekno.arab_project.Model.CompanyList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.tekno.arab_project.Data.Activity.CompanyDetailActivity;
import com.example.tekno.arab_project.Data.Activity.MainActivity;
import com.example.tekno.arab_project.Data.Activity.MapActivity;
import com.example.tekno.arab_project.Data.ILoadMore;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.CompanyCommentList.LoadingViewHolder;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListResponseData;
import com.example.tekno.arab_project.R;

import java.io.Serializable;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity activity;
    private List<CompanyListResponseData> datas;
    private Util util;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ILoadMore LoadMore;
    boolean isLoading;
    boolean permissionCall;
    int visibleThreshold = 5;
    int lastVisibleItem, totalItemCount;

    public ItemAdapter(RecyclerView recyclerView, Activity activity, List<CompanyListResponseData> datas) {
        this.activity = activity;
        this.datas = datas;

        util=new Util(activity);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (LoadMore != null) {
                        LoadMore.onLoadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void setLoadMore(ILoadMore LoadMore) {
        this.LoadMore = LoadMore;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_company, parent, false);
            final ItemHolder itemHolder = new ItemHolder(itemView, activity);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = itemHolder.getAdapterPosition();
                    Intent intentDetail = new Intent(activity, CompanyDetailActivity.class);
                    intentDetail.putExtra("companyDetail", (Serializable) datas.get(position).getCompanyInformation());
                    activity.startActivity(intentDetail);
                }
            });
            return itemHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemHolder) {
            final CompanyListResponseData data = datas.get(position);
            final ItemHolder itemHolder = (ItemHolder) holder;
            itemHolder.companyName.setText(data.getCompanyInformation().getTitle());
            itemHolder.companyRate.setText(data.getCompanyInformation().getRating().toString());
            itemHolder.companyRateBar.setRating(Float.valueOf(String.valueOf(data.getCompanyInformation().getRating())));
            itemHolder.companyPhone.setText(data.getCompanyInformation().getPhone1());
            ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(activity, data.getCompanyGallery(), data.getCompanyInformation(),itemHolder.progressCompanyPhoto);
            itemHolder.viewPager.setAdapter(imageSliderAdapter);
            itemHolder.indicator.setViewPager(itemHolder.viewPager);
            if (!TextUtils.isEmpty(data.getCompanyInformation().getLogoPath())) {
                Glide.with(activity)
                        .load(data.getCompanyInformation().getLogoPath())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                itemHolder.progressCompanyLogo.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                itemHolder.progressCompanyLogo.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(itemHolder.companyLogo);
            }

            itemHolder.mapShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentMap = new Intent(activity, MapActivity.class);
                    intentMap.putExtra("companyInMap", data.getCompanyInformation());
                    activity.startActivity(intentMap);
                }
            });
            itemHolder.companyPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    permissionCall=util.checkPermissionCall(activity);
                    if(permissionCall){
                        String number = itemHolder.companyPhone.getText().toString();
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + number));
                        activity.startActivity(intent);
                        //util.getCallDialog(activity,itemHolder.companyName.getText().toString(),itemHolder.companyPhone.getText().toString());
                    }
                    else{
                        String msg = activity.getResources().getString(R.string.dialog_call_permission_message);
                        util.getDialogInformation(activity, msg, false);
                    }
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public void setLoaded() {
        isLoading = false;
    }

}
