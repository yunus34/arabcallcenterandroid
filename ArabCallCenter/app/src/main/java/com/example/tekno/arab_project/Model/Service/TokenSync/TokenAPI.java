package com.example.tekno.arab_project.Model.Service.TokenSync;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface TokenAPI {
    @FormUrlEncoded
    @POST("token")
    Call<TokenResponse> token(
            @Field("username") String username,
            @Field("password")String password,
            @Field("grant_type") String grant_type
    );
}
