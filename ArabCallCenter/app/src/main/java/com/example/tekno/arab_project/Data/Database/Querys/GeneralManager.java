package com.example.tekno.arab_project.Data.Database.Querys;

import android.view.Menu;

import com.example.tekno.arab_project.Data.Database.DB_Helper;
import com.example.tekno.arab_project.Data.Database.MenuList;
import com.example.tekno.arab_project.Data.Database.Parameters;
import com.example.tekno.arab_project.Data.Database.Users;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class GeneralManager {
    private DB_Helper db_helper;

    public GeneralManager(DB_Helper db_helper) {
        this.db_helper = db_helper;
    }

    public List<Users> retrieveUser(String email,String password) throws SQLException {
        Dao<Users, UUID> usersDao=db_helper.getDao(Users.class);
        QueryBuilder<Users,UUID> uQb = usersDao.queryBuilder();
        uQb.where().eq(Users.IS_DELETED, false)
                .and().eq(Users.EMAIL,email)
                .and().eq(Users.PASSWORD,password);
        List<Users> results = uQb.query();
        return results;
    }
    public List<Parameters> retrieveParameters() throws SQLException {
        Dao<Parameters, UUID> parametersDao=db_helper.getDao(Parameters.class);
        QueryBuilder<Parameters,UUID> pQb = parametersDao.queryBuilder();
        pQb.where().eq(Parameters.IS_DELETED, false);
        List<Parameters> results = pQb.query();
        return results;
    }
    //Sonra kullanılacak
    public List<Parameters> retrieveParametersGroupName() throws SQLException {
        Dao<Parameters, UUID> parametersDao=db_helper.getDao(Parameters.class);
        QueryBuilder<Parameters,UUID> pQb = parametersDao.queryBuilder();
        pQb.where().eq(Parameters.IS_DELETED, false);
        pQb.where().eq(Parameters.GROUP_NAME, false);
        List<Parameters> results = pQb.query();
        return results;
    }
    public List<MenuList> retrieveMenuLists() throws SQLException {
        Dao<MenuList, UUID> usersDao=db_helper.getDao(MenuList.class);
        QueryBuilder<MenuList,UUID> uQb = usersDao.queryBuilder();
        uQb.where().eq(MenuList.IS_DELETED, false);
        List<MenuList> results = uQb.query();
        return results;
    }
    public List<MenuList> retrieveMenuListsById(UUID id) throws SQLException {
        Dao<MenuList, UUID> usersDao=db_helper.getDao(MenuList.class);
        QueryBuilder<MenuList,UUID> uQb = usersDao.queryBuilder();
        uQb.where().eq(MenuList.IS_DELETED, false)
        .and().eq(MenuList.ID,id);
        List<MenuList> results = uQb.query();
        return results;
    }
}
