package com.example.tekno.arab_project.Model.Service.UserUpdateSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserUpdateResponseData {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Surname")
    @Expose
    private String surname;
    @SerializedName("Password")
    @Expose
    private Object password;
    @SerializedName("IsBanned")
    @Expose
    private Object isBanned;
    @SerializedName("IsDeleted")
    @Expose
    private Object isDeleted;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CreateUser")
    @Expose
    private Object createUser;
    @SerializedName("UpdateUser")
    @Expose
    private Object updateUser;
    @SerializedName("UpdateDate")
    @Expose
    private Object updateDate;
    @SerializedName("GsmNo")
    @Expose
    private String gsmNo;
    @SerializedName("Assessment")
    @Expose
    private List<Object> assessment = null;
    @SerializedName("Complaint")
    @Expose
    private List<Object> complaint = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public Object getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(Object isBanned) {
        this.isBanned = isBanned;
    }

    public Object getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public String getGsmNo() {
        return gsmNo;
    }

    public void setGsmNo(String gsmNo) {
        this.gsmNo = gsmNo;
    }

    public List<Object> getAssessment() {
        return assessment;
    }

    public void setAssessment(List<Object> assessment) {
        this.assessment = assessment;
    }

    public List<Object> getComplaint() {
        return complaint;
    }

    public void setComplaint(List<Object> complaint) {
        this.complaint = complaint;
    }
}
