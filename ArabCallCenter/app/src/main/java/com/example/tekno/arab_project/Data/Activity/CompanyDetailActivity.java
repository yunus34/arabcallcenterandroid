package com.example.tekno.arab_project.Data.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Adapters.FeatureItemAdapter;
import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Enum.GalleryTypeId;
import com.example.tekno.arab_project.Data.GridViewScrollable;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.CompanyComment.ItemAdapter;
import com.example.tekno.arab_project.Model.CompanyComment.CompanyCommentListModel;
import com.example.tekno.arab_project.Model.FeatureListModel;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListResponse;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListAPI;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListModel;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListResponseData;
import com.example.tekno.arab_project.Model.Service.AssessmentListSync.AssessmentListRetrofitGET;
import com.example.tekno.arab_project.Model.Service.CompanyDetailSync.CompanyDetailAPI;
import com.example.tekno.arab_project.Model.Service.CompanyDetailSync.CompanyDetailGalleryResponse;
import com.example.tekno.arab_project.Model.Service.CompanyDetailSync.CompanyDetailResponse;
import com.example.tekno.arab_project.Model.Service.CompanyDetailSync.CompanyDetailResponseData;
import com.example.tekno.arab_project.Model.Service.CompanyDetailSync.CompanyDetailRetrofitGET;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListInformationResponse;
import com.example.tekno.arab_project.Model.Service.FeatureListSync.FeatureListAPI;
import com.example.tekno.arab_project.Model.Service.FeatureListSync.FeatureListResponse;
import com.example.tekno.arab_project.Model.Service.FeatureListSync.FeatureListResponseData;
import com.example.tekno.arab_project.Model.Service.FeatureListSync.FeatureListRetrofitGET;
import com.example.tekno.arab_project.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyDetailActivity extends YouTubeBaseActivity implements View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private static final String TAG=CompanyDetailActivity.class.getName();
    private static final String COMPANY_ID = "COMPANY_ID";
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private int CALL_PERMISSION_CODE = 101;
    private Toolbar toolbar;
    private CompanyDetailResponse companyDetailResponse;
    private CompanyListInformationResponse companyListInformationResponse;
    private CompanyDetailResponseData companyDetailResponseData;
    private AssessmentListResponse assessmentListResponse;
    private List<AssessmentListResponseData> assessmentListResponseData;
    private MapFragment mapview;
    private GoogleMap gMap;
    private RecyclerView commentListRcy;
    private TextView companyRate;
    private RatingBar ratingBar;
    private TextView companyName;
    private TextView companyDistrict;
    private TextView companyPhone;
    private TextView companyWebSite;
    private TextView companyAbout;
    private TextView companyAboutText;
    private TextView companyFeatureText;
    private TextView companyCommentText;
    private TextView noVideo;
    private TextView companyCommentAll;
    private CardView features;
    private CardView comments;
    private View viewLine;
    private ImageView imageView;
    private YouTubePlayerView videoView;

    private ImageView playIcon;
    private ImageView commentAdd;
    private GridViewScrollable featureGridView;
    private FeatureItemAdapter featureItemAdapter;
    private LinearLayoutManager layoutManager;
    private ItemAdapter itemAdapter;
    private List<CompanyCommentListModel> companyCommentListModels;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialogGetFrame;
    private Util util;
    private static String companyId;
    private String access;
    private Integer skip;
    private Integer take;
    private String[] latlng;
    private double lat;
    private double lng;
    private boolean videoCheck=false;
    public boolean permissionCall = false;
    private String videoURL;
    private String videoPath;

    private String[] featureJsonInfo=null;
    private FeatureListModel featureListModel;
    private List<FeatureListModel> featureListModels;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_detail);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.company_detail));
        toolbar.setTitle("");
        setActionBar(toolbar);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        Bundle bundle = getIntent().getExtras();
        progressDialog = new ProgressDialog(this);
        progressDialogGetFrame=new ProgressDialog(this);
        companyCommentListModels = new ArrayList<>();
        assessmentListResponseData = new ArrayList<>();
        featureListModels=new ArrayList<>();

        skip = Constants.SKIP;
        take = Constants.TAKE;

        if (bundle != null) {
            companyListInformationResponse = (CompanyListInformationResponse) bundle.getSerializable("companyDetail");
            companyId = companyListInformationResponse.getId();
        }
        SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");

        commentListRcy = (RecyclerView) findViewById(R.id.commentListRcy);
        companyRate = (TextView) findViewById(R.id.companyRate);
        companyRate.setTypeface(util.getTypefaceRate());
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        companyName = (TextView) findViewById(R.id.companyName);
        companyName.setTypeface(util.getTypefaceBold());

        companyDistrict = (TextView) findViewById(R.id.companyDistrict);
        companyDistrict.setTypeface(util.getTypeface());

        companyPhone = (TextView) findViewById(R.id.companyPhone);
        companyPhone.setTypeface(util.getTypeface());

        companyWebSite = (TextView) findViewById(R.id.companyWebSite);
        companyWebSite.setTypeface(util.getTypeface());

        companyAbout = (TextView) findViewById(R.id.companyAbout);
        companyAbout.setTypeface(util.getTypeface());

        companyAboutText = (TextView) findViewById(R.id.companyAboutText);
        companyAboutText.setTypeface(util.getTypeface());

        features=(CardView)findViewById(R.id.features);
        comments=(CardView)findViewById(R.id.comments);

        companyFeatureText=(TextView) findViewById(R.id.companyFeatureText);
        companyFeatureText.setTypeface(util.getTypeface());

        companyCommentText = (TextView) findViewById(R.id.companyCommentText);
        companyCommentText.setTypeface(util.getTypeface());

        companyCommentAll = (TextView) findViewById(R.id.companyCommentAll);
        companyCommentAll.setTypeface(util.getTypeface());

        imageView = (ImageView) findViewById(R.id.imageView);
        videoView = (YouTubePlayerView) findViewById(R.id.youtubeVideoView);
        noVideo=(TextView)findViewById(R.id.no_video);
        playIcon = (ImageView) findViewById(R.id.playIcon);
        commentAdd = (ImageView) findViewById(R.id.commentAdd);
        featureGridView=new GridViewScrollable(this);
        featureGridView=(GridViewScrollable)findViewById(R.id.gridviewFature);


        latlng = companyListInformationResponse.getLocationInfo().split(";");
        lat = Double.valueOf(latlng[0]);
        lng = Double.valueOf(latlng[1]);

        mapview = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

        mapview.getMapAsync(this);
        companyWebSite.setOnClickListener(this);
        companyPhone.setOnClickListener(this);
        commentAdd.setOnClickListener(this);
        companyCommentAll.setOnClickListener(this);
        playIcon.setOnClickListener(this);
        companyDetail();
    }

    private static Intent newIntent(Activity activity, String companyId) {
        Intent intent = new Intent(activity, CompanyComment.class);
        intent.putExtra(COMPANY_ID, companyId);
        return intent;
    }

    private void companyDetail() {

        progressDialog.show();
        Util.setProgressDialog(progressDialog);

        CompanyDetailAPI companyDetailRetrofitGET = CompanyDetailRetrofitGET.getRetrofitCompanyDetail().create(CompanyDetailAPI.class);
        Call<CompanyDetailResponse> call = companyDetailRetrofitGET.companyDetail(access, companyListInformationResponse.getId());
        call.enqueue(new Callback<CompanyDetailResponse>() {

            @Override
            public void onResponse(Call<CompanyDetailResponse> call, Response<CompanyDetailResponse> response) {
                companyDetailResponse = response.body();
                try{
                    if (companyDetailResponse != null) {
                        dataSet(companyDetailResponse);
                    }else{
                        Util.dismisProgressDialog(progressDialog);
                    }
                }catch (Exception e){
                    Log.e(TAG,"CompanyDetailResponse Check Error:"+ e.toString());
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<CompanyDetailResponse> call, Throwable t) {
                Log.e(TAG,"CompanyDetailResponse Error:"+ t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }

    private void dataSet(CompanyDetailResponse companyDetailResponse) {
        companyDetailResponseData = companyDetailResponse.getCompanyDetailResponseData();
        companyRate.setText(companyDetailResponseData.getCompanyDetailInformationResponse().getRating().toString());
        ratingBar.setRating(Float.valueOf(companyDetailResponseData.getCompanyDetailInformationResponse().getRating().toString()));
        companyName.setText(companyDetailResponseData.getCompanyDetailInformationResponse().getTitle());
        companyDistrict.setText(companyDetailResponseData.getCompanyDetailInformationResponse().getAddress());
        companyPhone.setText(companyDetailResponseData.getCompanyDetailInformationResponse().getPhone1());
        companyWebSite.setText(companyDetailResponseData.getCompanyDetailInformationResponse().getWebAddress());
        companyAbout.setText(companyDetailResponseData.getCompanyDetailInformationResponse().getDescription());
        if(companyDetailResponseData.getCompanyDetailInformationResponse().getJsonInfoData()!=null){
            featureJsonInfo= companyDetailResponseData.getCompanyDetailInformationResponse().getJsonInfoData().split(";");
        }

        //Videodan belirli saniyede bir kare almak
        for (CompanyDetailGalleryResponse companyDetailGalleryResponse : companyDetailResponseData.getCompanyGalleries()) {
            if(companyDetailGalleryResponse.getGalleryTypeId() == GalleryTypeId.Video.getValue()) {
                if(!TextUtils.isEmpty(companyDetailGalleryResponse.getGalleryPath())){
                    videoURL=companyDetailGalleryResponse.getGalleryPath();
                    //new GetVideoFrame(companyDetailGalleryResponse.getGalleryPath()).execute();
                    noVideo.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);

                }else{
                    videoView.setVisibility(View.GONE);
                }
            }
        }
        assessmentList();
    }

    private void assessmentList() {

        AssessmentListModel assesmentListModel = new AssessmentListModel(companyListInformationResponse.getId(), Constants.SKIP, Constants.TAKE);
        SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        String access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");
        AssessmentListAPI assessmentListAPI = AssessmentListRetrofitGET.getRetrofitAssessmentList().create(AssessmentListAPI.class);
        Call<AssessmentListResponse> call = assessmentListAPI.assessmentList(access, assesmentListModel);
        call.enqueue(new Callback<AssessmentListResponse>() {
            @Override
            public void onResponse(Call<AssessmentListResponse> call, Response<AssessmentListResponse> response) {
                assessmentListResponse = response.body();
                    if (assessmentListResponse != null) {
                        comments.setVisibility(View.VISIBLE);
                        assessmentListResponseData = assessmentListResponse.getResponseData();
                        dataList(companyCommentListModels, assessmentListResponseData);
                    }else{
                        Util.dismisProgressDialog(progressDialog);
                        comments.setVisibility(View.GONE);
                    }
            }

            @Override
            public void onFailure(Call<AssessmentListResponse> call, Throwable t) {
                Log.e(TAG,"AssessmentListResponse Error:"+ t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }

    private void dataList(final List<CompanyCommentListModel> companyCommentListModels, List<AssessmentListResponseData> assessmentListResponseData) {
        for (AssessmentListResponseData assessmentData : assessmentListResponseData) {
            companyCommentListModels.add(new CompanyCommentListModel(util.getDateCurrent(), assessmentData.getRate(), assessmentData.getComment()));
        }
        if (companyCommentListModels.size() > 2) {
            companyCommentAll.setVisibility(View.VISIBLE);
        }
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayout.VERTICAL);
        commentListRcy.setLayoutManager(layoutManager);
        itemAdapter = new ItemAdapter(this, companyCommentListModels);
        commentListRcy.setAdapter(itemAdapter);
        commentListRcy.setItemAnimator(new DefaultItemAnimator());
        commentListRcy.setHasFixedSize(true);
        commentListRcy.setFocusable(false);

        featureList();

    }

    private void featureList() {


        final FeatureListAPI featureListAPI= FeatureListRetrofitGET.getRetrofitFeatureList().create(FeatureListAPI.class);
        Observable<FeatureListResponse> call=featureListAPI.featureList(access);
        call.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.Observer<FeatureListResponse>() {
                               @Override
                               public void onSubscribe(Disposable d) {

                               }

                               @Override
                               public void onNext(FeatureListResponse featureListResponse) {
                                   if(featureListResponse!=null){
                                       comments.setVisibility(View.VISIBLE);
                                       if(featureJsonInfo!=null && featureListResponse.getResponseData()!=null){
                                           features.setVisibility(View.VISIBLE);
                                           for(String featureItem:featureJsonInfo){
                                               Integer feature=Integer.parseInt(featureItem);
                                               for(FeatureListResponseData featureListResponseData:featureListResponse.getResponseData())
                                                   if(feature==featureListResponseData.getId()){
                                                       featureListModels.add(new FeatureListModel(featureListResponseData.getName(),featureListResponseData.getIconPath()));
                                                   }
                                           }
                                       }else{
                                           features.setVisibility(View.GONE);
                                       }
                                   }
                               }

                               @Override
                               public void onError(Throwable e) {
                               }

                               @Override
                               public void onComplete() {
                                   featureListFill(featureListModel);
                               }
                           });

    }

    private void featureListFill(FeatureListModel featureListModel) {
        featureItemAdapter=new FeatureItemAdapter(this,featureListModels);
        featureGridView.setAdapter(featureItemAdapter);
        featureGridView.setExpanded(true);
        youtubeVideo();
        Util.dismisProgressDialog(progressDialog);
    }

    private void youtubeVideo() {
        if(!TextUtils.isEmpty(videoURL)){
            videoPath=videoURL.substring(30);
            onInitializedListener=new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    youTubePlayer.cueVideo(videoPath);
                }
                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                }
            };
            videoView.initialize(Constants.YOUTUBE_API_KEY,onInitializedListener);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.companyWebSite:
                try {
                    if (!companyWebSite.equals("")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse("http://" + companyWebSite.getText().toString().trim()));
                        startActivity(browserIntent);
                        break;
                    }
                } catch (Exception e) {

                }
            case R.id.companyPhone:
                permissionCall=util.checkPermissionCall(this);
                if(permissionCall){
                    String number = companyDetailResponseData.getCompanyDetailInformationResponse().getPhone1();
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + number));
                    startActivity(intent);
                    //util.getCallDialog(this,companyDetailResponseData.getCompanyDetailInformationResponse().getTitle(),companyDetailResponseData.getCompanyDetailInformationResponse().getPhone1());
                }
                else{
                    String msg = this.getResources().getString(R.string.dialog_call_permission_message);
                    util.getDialogInformation(this, msg, false);
                }
                break;
            /*case R.id.playIcon:
                Intent intentVideo = new Intent(this, VideoViewActivity.class);
                intentVideo.putExtra("VideoURL",videoURL);
                startActivity(intentVideo);
                break;*/

            case R.id.commentAdd:
                Intent intent = CompanyDetailActivity.newIntent(this, companyId);
                startActivity(intent);
                break;

            case R.id.companyCommentAll:
                Intent intentCommentAll = new Intent(this, CompanyCommentList.class);
                intentCommentAll.putExtra("companyId", companyId);
                startActivity(intentCommentAll);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {
        if (requestCode == CALL_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permissionCall = true;
            } else {
                permissionCall = false;
        }
    }
    }

    public class GetVideoFrame extends AsyncTask<Void, Bitmap, Bitmap> {

        private String videoPath;

        public GetVideoFrame(String videoPath) {
            this.videoPath = videoPath;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            Bitmap bitmap = null;
            //Bitmap bitmap = util.getImageUrl("https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4");
            try{
                bitmap = util.getImageUrl(videoPath);
            }catch (Exception e){
                bitmap=null;
                Log.i(TAG,"Get Video Frame Error:"+ e.toString());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if(bitmap!=null){
                //Glide.with(CompanyDetailActivity.this).load(bitmap).into(videoImageView);
            }
            assessmentList();
            Util.dismisProgressDialog(progressDialog);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        try{
            BitmapDescriptor icon = Util.bitmapDescriptorFromVector(this, R.drawable.ic_location);
            Marker marker = gMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .title(companyListInformationResponse.getTitle())
                    .icon(icon)
            );
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.0f));

            gMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    Intent intent = MapActivity.newIntent(CompanyDetailActivity.this, latlng, 1, companyListInformationResponse.getTitle());
                    startActivity(intent);
                }
            });

        }catch(Exception e){
            Log.e(TAG,"Map lat lng error:"+e.toString());
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
