package com.example.tekno.arab_project.Data.Database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MenuList")
public class MenuList extends BaseEntity {
    public static final String NAME = "Name";
    public static final String ICON_PATH = "IconPath";
    public static final String JSON_INFO = "JsonInfo";
    public static final String CREATE_USER = "CreateUser";
    public static final String UPDATE_USER = "UpdateUser";
    public static final String SEQUENCE = "Sequence";

    @DatabaseField(columnName = NAME)
    private String name;

    @DatabaseField(columnName = ICON_PATH)
    private String iconPath;

    @DatabaseField(columnName = JSON_INFO)
    private String jsonInfo;

    @DatabaseField(columnName = CREATE_USER)
    private String createUser;

    @DatabaseField(columnName = UPDATE_USER)
    private String updateUser;

    @DatabaseField(columnName = SEQUENCE)
    private Integer sequence;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getJsonInfo() {
        return jsonInfo;
    }

    public void setJsonInfo(String jsonInfo) {
        this.jsonInfo = jsonInfo;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
}
