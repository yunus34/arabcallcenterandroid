package com.example.tekno.arab_project.Model.CompanyList;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Data.Activity.MapActivity;
import com.example.tekno.arab_project.R;
import com.pixelcan.inkpageindicator.InkPageIndicator;

public class ItemHolder extends RecyclerView.ViewHolder {
    public TextView companyRate;
    public TextView companyName;
    public TextView companyPhone;
    public RatingBar companyRateBar;
    public TextView mapShow;
    public ProgressBar progressCompanyLogo;
    public ProgressBar progressCompanyPhoto;
    public ViewPager viewPager;
    public InkPageIndicator indicator;
    public ImageView companyLogo;
    public Context context;
    private Util util;

    public ItemHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        this.util = new Util(context);

        companyRate = (TextView) itemView.findViewById(R.id.companyRate);
        companyRate.setTypeface(util.getTypefaceRate());
        companyName = (TextView) itemView.findViewById(R.id.companyName);
        companyName.setTypeface(util.getTypeface());
        companyRateBar = (RatingBar) itemView.findViewById(R.id.companyRateBar);
        companyPhone = (TextView) itemView.findViewById(R.id.companyPhone);
        companyPhone.setTypeface(util.getTypeface());
        mapShow = (TextView) itemView.findViewById(R.id.mapShow);
        mapShow.setTypeface(util.getTypeface());
        viewPager = (ViewPager) itemView.findViewById(R.id.viewPager);
        indicator = (InkPageIndicator) itemView.findViewById(R.id.indicator);
        companyLogo = (ImageView) itemView.findViewById(R.id.companyLogo);
        progressCompanyLogo = (ProgressBar) itemView.findViewById(R.id.companyLogoProgress);

    }

}
