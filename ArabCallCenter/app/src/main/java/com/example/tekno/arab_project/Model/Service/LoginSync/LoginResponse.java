package com.example.tekno.arab_project.Model.Service.LoginSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("responseName")
    @Expose
    private String responseName;
    @SerializedName("responseData")
    @Expose
    private LoginResponseUserData loginResponseUserData;
    @SerializedName("error")
    @Expose
    private Object error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public LoginResponseUserData getLoginResponseUserData() {
        return loginResponseUserData;
    }

    public void setLoginResponseUserData(LoginResponseUserData loginResponseUserData) {
        this.loginResponseUserData = loginResponseUserData;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}
