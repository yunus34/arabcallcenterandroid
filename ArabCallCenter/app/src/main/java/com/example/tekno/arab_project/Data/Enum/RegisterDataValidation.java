package com.example.tekno.arab_project.Data.Enum;

public enum RegisterDataValidation {
    Email(1),
    Password(2),
    Phone(3),
    GoogleSignIn(4),
    FacebookSignIn(5),
    UserRequired(7);

    private int valid;

    RegisterDataValidation(int valid) {
        this.valid = valid;
    }

    public int getValid() {
        return valid;
    }
}
