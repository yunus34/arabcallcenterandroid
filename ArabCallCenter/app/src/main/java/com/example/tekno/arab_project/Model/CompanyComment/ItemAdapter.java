package com.example.tekno.arab_project.Model.CompanyComment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tekno.arab_project.R;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemHolder> {

    private Context context;
    private List<CompanyCommentListModel> companyCommentListModels;

    public ItemAdapter(Context context,List<CompanyCommentListModel> companyCommentListModels) {
        this.context = context;
        this.companyCommentListModels = companyCommentListModels;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.company_comment_list_item, parent, false);

        return new ItemHolder(itemView,context);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        CompanyCommentListModel data=companyCommentListModels.get(position);
        holder.commentDate.setText(data.getCommentDate());
        holder.ratingBar.setRating(Float.valueOf(data.getCommentRate()));
        holder.commentText.setText(data.getCommentText());

    }

    @Override
    public int getItemCount() {
        if (companyCommentListModels.size() == 2 || companyCommentListModels.size() >= 2)
            return 2;

        return companyCommentListModels.size();
    }
}
