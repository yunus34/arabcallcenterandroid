package com.example.tekno.arab_project.Model.Service.MenuControllerSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuControllerResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("responseName")
    @Expose
    private String responseName;
    @SerializedName("responseData")
    @Expose
    private MenuControllerResponseData responseData;
    @SerializedName("error")
    @Expose
    private Object error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public MenuControllerResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(MenuControllerResponseData responseData) {
        this.responseData = responseData;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}
