package com.example.tekno.arab_project.Model.Service.MenuControllerSync;

public class MenuControllerModel {
    public String lastUpdateDate;

    public MenuControllerModel(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
