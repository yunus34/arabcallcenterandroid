package com.example.tekno.arab_project.Model.Service.MenuListSync;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MenuListAPI {
    @POST("List")
    Call<MenuListResponse> menuList(
            @Header("Authorization") String auth
    );
}