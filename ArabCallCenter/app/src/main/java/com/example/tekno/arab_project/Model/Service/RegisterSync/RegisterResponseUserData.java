package com.example.tekno.arab_project.Model.Service.RegisterSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.UUID;

public class RegisterResponseUserData {

    @SerializedName("Id")
    @Expose
    private UUID id;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Surname")
    @Expose
    private String surname;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("IsBanned")
    @Expose
    private Boolean isBanned;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CreateUser")
    @Expose
    private String createUser;
    @SerializedName("UpdateUser")
    @Expose
    private String updateUser;
    @SerializedName("UpdateDate")
    @Expose
    private String updateDate;
    @SerializedName("GsmNo")
    @Expose
    private String gsmNo;
    @SerializedName("Assessment")
    @Expose
    private List<Object> assessment = null;
    @SerializedName("Complaint")
    @Expose
    private List<Object> complaint = null;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getBanned() {
        return isBanned;
    }

    public void setBanned(Boolean banned) {
        isBanned = banned;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getGsmNo() {
        return gsmNo;
    }

    public void setGsmNo(String gsmNo) {
        this.gsmNo = gsmNo;
    }

    public List<Object> getAssessment() {
        return assessment;
    }

    public void setAssessment(List<Object> assessment) {
        this.assessment = assessment;
    }

    public List<Object> getComplaint() {
        return complaint;
    }

    public void setComplaint(List<Object> complaint) {
        this.complaint = complaint;
    }
}
