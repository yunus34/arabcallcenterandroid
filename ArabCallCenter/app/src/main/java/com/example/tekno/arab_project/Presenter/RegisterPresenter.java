package com.example.tekno.arab_project.Presenter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Database.DB_Helper;
import com.example.tekno.arab_project.Data.Database.Users;
import com.example.tekno.arab_project.Data.Enum.RegisterDataValidation;
import com.example.tekno.arab_project.Data.Fragment.RegisterFragment;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterAPI;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterModel;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterResponse;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterResponseError;
import com.example.tekno.arab_project.Model.Service.RegisterSync.RegisterRetrofitGET;
import com.example.tekno.arab_project.R;
import com.example.tekno.arab_project.View.LoginView;
import com.example.tekno.arab_project.View.RegisterView;

import java.sql.SQLException;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class RegisterPresenter implements IRegisterPresenter{
    private static final String TAG = RegisterFragment.class.getName();
    private DB_Helper db_helper;
    private RegisterView registerView;
    private LoginView loginView;
    private ProgressDialog progressDialog;
    private Users users;
    private RegisterResponse response;
    private RegisterResponseError registerResponseError;

    public RegisterPresenter(RegisterView registerView) {
        this.registerView = registerView;

    }
    public RegisterPresenter(LoginView loginView, boolean social) {
        this.loginView = loginView;

    }

    @Override
    public void performRegister(final Activity activity, String userMail, String userPassword, String gsmNo) {
        db_helper = new DB_Helper(activity);
        users = new Users();
        response = new RegisterResponse();

        RegisterModel registerModel = new RegisterModel(userMail, userPassword, gsmNo,"");
        boolean isEmailSuccess = registerModel.isEmailValidData();
        boolean isPasswordSuccess = registerModel.isPasswordValidData();
        boolean isPhoneSuccess = registerModel.isPhoneValidData();

        if (!isEmailSuccess) {
            registerView.registerError(RegisterDataValidation.Email.getValid());
        } else if (!isPasswordSuccess) {
            registerView.registerError(RegisterDataValidation.Password.getValid());
        } else {

            progressDialog = new ProgressDialog(activity);
            progressDialog.show();
            Util.setProgressDialog(progressDialog);

            try {
                RegisterAPI registerAPI = RegisterRetrofitGET.getRetrofit().create(RegisterAPI.class);
                Observable<RegisterResponse> call = registerAPI.register(Constants.ApplicationSignature, registerModel);
                call.subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new io.reactivex.Observer<RegisterResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                            }

                            @Override
                            public void onNext(RegisterResponse registerResponse) {
                                response = registerResponse;

                                if (registerResponse != null && registerResponse.getSuccess()) {
                                    users.setId(registerResponse.getRegisterResponseUserData().getId());
                                    users.setDeleted(registerResponse.getRegisterResponseUserData().getDeleted());
                                    users.setCreateDate(registerResponse.getRegisterResponseUserData().getCreateDate());
                                    users.setUpdateDate(registerResponse.getRegisterResponseUserData().getUpdateDate());

                                    users.setUserEmail(registerResponse.getRegisterResponseUserData().getEmail());
                                    users.setUserName(registerResponse.getRegisterResponseUserData().getName());
                                    users.setUserSurName(registerResponse.getRegisterResponseUserData().getSurname());
                                    users.setUserPassword(registerResponse.getRegisterResponseUserData().getPassword());
                                    users.setUserPhone(registerResponse.getRegisterResponseUserData().getGsmNo());
                                    users.setUserIsBanned(registerResponse.getRegisterResponseUserData().getBanned());
                                    try {
                                        db_helper.getDao(Users.class).create(users);
                                    } catch (SQLException e) {
                                        Log.d(TAG, e.toString());
                                        registerView.registerDatabaseError(e);
                                    }
                                } else {
                                    registerResponseError = registerResponse.getError();
                                    if (registerResponseError.getErrorCode() == RegisterDataValidation.UserRequired.getValid())
                                        registerView.registerError(RegisterDataValidation.UserRequired.getValid());
                                }

                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "RegisterResponse Error:" + e.toString());
                                Toasty.error(activity, activity.getResources().getText(R.string.validate_register), Toast.LENGTH_LONG).show();
                                Util.dismisProgressDialog(progressDialog);
                            }

                            @Override
                            public void onComplete() {
                                if (response.getSuccess().booleanValue()) {
                                    registerView.registerSuccess();
                                    Util.dismisProgressDialog(progressDialog);
                                } else {
                                    Toasty.error(activity, activity.getResources().getText(R.string.validate_register_before_signed), Toast.LENGTH_LONG).show();
                                    Util.dismisProgressDialog(progressDialog);
                                }
                            }
                        });
            } catch (Exception e) {
                Log.e(TAG, "Register Error:" + e.toString());
            }
        }
    }
}

