package com.example.tekno.arab_project.Data;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Activity.MainActivity;
import com.example.tekno.arab_project.R;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class Util {
    private int CALL_PERMISSION_CODE = 101;
    private Context context;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 100;

    public Util(Context context) {
        this.context = context;
    }

    public Typeface getTypeface() {
        Typeface newTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Light.ttf");
        return newTypeface;
    }

    public Typeface getTypefaceBold() {
        Typeface newTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
        return newTypeface;
    }
    public Typeface getTypefaceRate() {
        Typeface newTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/AutourOne-Regular.ttf");
        return newTypeface;
    }


    public String getDateCurrent() {
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String date = df.format(currentTime);
        return date;
    }

    public static boolean isEmpty(EditText edittext) {
        if (edittext.getText().toString().trim().length() > 0)
            return false;

        return true;
    }
    public static boolean isSet(String param) {
        // doesn't ignore spaces, but does save an object creation.
        return param != null && param.length() != 0;
    }
    public static boolean isEmailValidData(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Bitmap getImageUrl(String url) {
        MediaMetadataRetriever media = new MediaMetadataRetriever();
        media.setDataSource(url, new HashMap<String, String>());
        Bitmap extractedImage = media.getFrameAtTime(2000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);

        return extractedImage;
    }

    public static void setProgressDialog(ProgressDialog progressDialog){
        progressDialog.setContentView(R.layout.dialog_layout_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);
    }
    public static void dismisProgressDialog(ProgressDialog progressDialog){
        if(progressDialog!=null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
    public static String getCurrentDate(){
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdfDate.format(currentTime);
        return strDate;
    }
    public static String getCurrentDateMilles(){
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String strDate = sdfDate.format(currentTime);
        return strDate;
    }
    public void getDialogInformation(final Activity activity, String msg, final Boolean fromActivity){
        final Dialog dialog=new Dialog(activity);
        dialog.setContentView(R.layout.dialog_location_information);
        dialog.setCancelable(true);
        TextView information_text=(TextView)dialog.findViewById(R.id.information_text);
        information_text.setTypeface(getTypefaceBold());
        TextView message=(TextView)dialog.findViewById(R.id.information_message);
        message.setTypeface(getTypeface());
        message.setText(msg);
        TextView btn_ok=(TextView)dialog.findViewById(R.id.btn_ok);
        btn_ok.setTypeface(getTypeface());
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fromActivity){Intent intent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    activity.startActivity(intent);
                    dialog.dismiss();
                }
                else{
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }
    public void getCallDialog(final Activity activity, String title, String number){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_soluation_center, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        alertDialog.setCancelable(true);
        final TextView soluation_center_text = (TextView) dialogView.findViewById(R.id.arab_call_center_aoluation_center);
        soluation_center_text.setTypeface(getTypeface());
        soluation_center_text.setText(title);
        final TextView soluation_center_number = (TextView) dialogView.findViewById(R.id.soluation_center_number);
        soluation_center_number.setTypeface(getTypeface());
        soluation_center_number.setText(number);
        Button dialogButton = (Button) dialogView.findViewById(R.id.soluation_center_button);
        dialogButton.setTypeface(getTypeface());

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String number = soluation_center_number.getText().toString().trim();
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + number));
                activity.startActivity(intent);

        }
        });
        alertDialog.show();
    }

    public static String getCurrentMinDate(){
        String minDate = "0001-01-01";
        return minDate;
    }
    public static BitmapDescriptor bitmapDescriptorFromVector (Context context, int vectorResId){
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    public static OkHttpClient getTimeOut(){
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        return okHttpClient;
    }


    public static boolean checkAndRequestPermissions(Activity activity) {
        int permissionINTERNET = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.INTERNET);
        int permissionACCESS_FINE_LOCATION = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCAMERA = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA);
        int permissionACCESS_COARSE_LOCATION = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int permissionWRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionREAD_PHONE_STATE = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_PHONE_STATE);
        int permissionCALL_PHONE = ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionINTERNET != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.INTERNET);
        }
        if (permissionCALL_PHONE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }
        if (permissionACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (permissionACCESS_COARSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (permissionINTERNET != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.INTERNET);
        }

        if (permissionWRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (permissionREAD_PHONE_STATE != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    public boolean checkPermissionCall(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(activity);
        } else {
            return true;
        }
        return false;
    }

    public void requestPermission(Activity activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSION_CODE);
        }
    }

}
