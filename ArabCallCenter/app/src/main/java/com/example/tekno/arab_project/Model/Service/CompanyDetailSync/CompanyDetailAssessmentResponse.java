package com.example.tekno.arab_project.Model.Service.CompanyDetailSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyDetailAssessmentResponse {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CompanyName")
    @Expose
    private Object companyName;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("UserNameSurname")
    @Expose
    private String userNameSurname;
    @SerializedName("AssessmentTypeId")
    @Expose
    private Integer assessmentTypeId;
    @SerializedName("UserId")
    @Expose
    private String userId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUserNameSurname() {
        return userNameSurname;
    }

    public void setUserNameSurname(String userNameSurname) {
        this.userNameSurname = userNameSurname;
    }

    public Integer getAssessmentTypeId() {
        return assessmentTypeId;
    }

    public void setAssessmentTypeId(Integer assessmentTypeId) {
        this.assessmentTypeId = assessmentTypeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
