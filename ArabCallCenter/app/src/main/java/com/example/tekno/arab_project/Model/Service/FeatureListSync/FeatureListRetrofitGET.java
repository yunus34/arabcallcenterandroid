package com.example.tekno.arab_project.Model.Service.FeatureListSync;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Util;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class FeatureListRetrofitGET {
    private static Retrofit retrofit=null;
    private static String BASE_URL_FEATURE_LIST= Constants.Feature_List_URL;
    public static Retrofit getRetrofitFeatureList()
    {
        if(retrofit==null)
        {
            retrofit=new Retrofit.Builder()
                    .baseUrl(BASE_URL_FEATURE_LIST)
                    .client(Util.getTimeOut())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }
        return retrofit;
    }
}
