package com.example.tekno.arab_project.Model.Service.CompanyListSync;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyListResponseData implements Parcelable {
    @SerializedName("CompanyInformation")
    @Expose
    private CompanyListInformationResponse companyInformation;
    @SerializedName("CompanyGallery")
    @Expose
    private List<CompanyListGalleryResponse> companyGallery = null;

    protected CompanyListResponseData(Parcel in) {
    }

    public static final Creator<CompanyListResponseData> CREATOR = new Creator<CompanyListResponseData>() {
        @Override
        public CompanyListResponseData createFromParcel(Parcel in) {
            return new CompanyListResponseData(in);
        }

        @Override
        public CompanyListResponseData[] newArray(int size) {
            return new CompanyListResponseData[size];
        }
    };

    public CompanyListInformationResponse getCompanyInformation() {
        return companyInformation;
    }

    public void setCompanyInformation(CompanyListInformationResponse companyInformation) {
        this.companyInformation = companyInformation;
    }

    public List<CompanyListGalleryResponse> getCompanyGallery() {
        return companyGallery;
    }

    public void setCompanyGallery(List<CompanyListGalleryResponse> companyGallery) {
        this.companyGallery = companyGallery;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
