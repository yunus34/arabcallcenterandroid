package com.example.tekno.arab_project.Model.Service.AssessmentInsertSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssessmentInsertResponseData {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("CompanyId")
    @Expose
    private String companyId;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("Rate")
    @Expose
    private Integer rate;
    @SerializedName("AssesmentTypeId")
    @Expose
    private Integer assesmentTypeId;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CreateUser")
    @Expose
    private String createUser;
    @SerializedName("UpdateUser")
    @Expose
    private Object updateUser;
    @SerializedName("UpdateDate")
    @Expose
    private Object updateDate;
    @SerializedName("Parameter")
    @Expose
    private Object parameter;
    @SerializedName("User")
    @Expose
    private Object user;
    @SerializedName("Company")
    @Expose
    private Object company;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getAssesmentTypeId() {
        return assesmentTypeId;
    }

    public void setAssesmentTypeId(Integer assesmentTypeId) {
        this.assesmentTypeId = assesmentTypeId;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public Object getParameter() {
        return parameter;
    }

    public void setParameter(Object parameter) {
        this.parameter = parameter;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public Object getCompany() {
        return company;
    }

    public void setCompany(Object company) {
        this.company = company;
    }
}
