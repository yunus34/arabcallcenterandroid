package com.example.tekno.arab_project.Model.Service.AssessmentInsertSync;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AssessmentInsertAPI {
    @POST("Insert")
    Call<AssesmentInsertResponse> assessmentInsert(
            @Header("Authorization") String auth,
            @Body AssessmentInsertModel assessmentInsertModel
    );
}
