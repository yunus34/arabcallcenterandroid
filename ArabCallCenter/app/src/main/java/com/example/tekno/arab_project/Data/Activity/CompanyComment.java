package com.example.tekno.arab_project.Data.Activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.Service.AssessmentInsertSync.AssesmentInsertResponse;
import com.example.tekno.arab_project.Model.Service.AssessmentInsertSync.AssesmentInsertRestrofitGET;
import com.example.tekno.arab_project.Model.Service.AssessmentInsertSync.AssessmentInsertAPI;
import com.example.tekno.arab_project.Model.Service.AssessmentInsertSync.AssessmentInsertModel;
import com.example.tekno.arab_project.R;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompanyComment extends AppCompatActivity implements View.OnClickListener {
    private static final String COMPANY_ID="COMPANY_ID";
    private Toolbar toolbar;
    private AssesmentInsertResponse assesmentInsertResponse;
    private TextView rateText;
    private TextView commentText;
    private TextView commentEnterText;
    private Button commentSend;
    private RatingBar ratingBar;
    private EditText comment;
    private Util util;
    private String companyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_comment);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title=(TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.comment_inset));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            companyId=bundle.getString(COMPANY_ID);
        }

        comment=(EditText)findViewById(R.id.commentEnterText);
        ratingBar=(RatingBar)findViewById(R.id.rate);

        rateText = (TextView) findViewById(R.id.rateText);
        rateText.setTypeface(util.getTypeface());
        commentText = (TextView) findViewById(R.id.commentText);
        commentText.setTypeface(util.getTypeface());
        commentEnterText = (TextView) findViewById(R.id.commentEnterText);
        commentEnterText.setTypeface(util.getTypeface());
        commentSend = (Button) findViewById(R.id.commentSend);
        commentSend.setTypeface(util.getTypeface());
        commentSend.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(ratingBar.getRating()<1){
            Toasty.error(CompanyComment.this,"You must give at least one star", Toast.LENGTH_LONG).show();
        }else if(Util.isEmpty(comment)){
            Toasty.error(CompanyComment.this,"You can't pass the comment field blank", Toast.LENGTH_LONG).show();
        }else{
            dataSend();
        }
    }
    private void dataSend() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
        Util.setProgressDialog(progressDialog);
        SharedPreferences sharedPrefToken = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        String access = Constants.TOKEN_TYPE + " " + sharedPrefToken.getString("access_token", "");

        SharedPreferences sharedPrefUserId = getSharedPreferences(Constants.USER_INFORMATIONS, 0);
        String userId = sharedPrefUserId.getString("UserId", "");
        AssessmentInsertModel assessmentInsertModel=new AssessmentInsertModel(userId,companyId, Constants.ASSESSMENT_TYPE_ID,(int)Math.round(ratingBar.getRating()),comment.getText().toString().trim());

        final AssessmentInsertAPI assessmentInsertAPI = AssesmentInsertRestrofitGET.getRetrofitAssessmentInsert().create(AssessmentInsertAPI.class);
        Call<AssesmentInsertResponse> call = assessmentInsertAPI.assessmentInsert(access, assessmentInsertModel);
        call.enqueue(new Callback<AssesmentInsertResponse>() {
            @Override
            public void onResponse(Call<AssesmentInsertResponse> call, Response<AssesmentInsertResponse> response) {
                assesmentInsertResponse=response.body();
                if(assesmentInsertResponse!=null && assesmentInsertResponse.getSuccess()){
                    Toasty.success(CompanyComment.this,"Send Comment Success", Toast.LENGTH_LONG).show();
                    Util.dismisProgressDialog(progressDialog);
                    finish();
                }else{
                    Util.dismisProgressDialog(progressDialog);
                    Toasty.error(CompanyComment.this,"Send Comment Error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AssesmentInsertResponse> call, Throwable t) {
                Log.i("AssesmentInsertResponse Error", t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }
}
