package com.example.tekno.arab_project.Model.Service.CompanyDetailSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyDetailGalleryResponse {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("GalleryTypeId")
    @Expose
    private Integer galleryTypeId;
    @SerializedName("GalleryPath")
    @Expose
    private String galleryPath;
    @SerializedName("CompanyId")
    @Expose
    private String companyId;
    @SerializedName("IsDeleted")
    @Expose
    private Object isDeleted;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CreateUser")
    @Expose
    private Object createUser;
    @SerializedName("UpdateUser")
    @Expose
    private Object updateUser;
    @SerializedName("UpdateDate")
    @Expose
    private Object updateDate;
    @SerializedName("SourceType")
    @Expose
    private Integer sourceType;
    @SerializedName("Parameter")
    @Expose
    private Object parameter;
    @SerializedName("Parameter1")
    @Expose
    private Object parameter1;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getGalleryTypeId() {
        return galleryTypeId;
    }

    public void setGalleryTypeId(Integer galleryTypeId) {
        this.galleryTypeId = galleryTypeId;
    }

    public String getGalleryPath() {
        return galleryPath;
    }

    public void setGalleryPath(String galleryPath) {
        this.galleryPath = galleryPath;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Object getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Object isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }

    public Object getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Object updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public Object getParameter() {
        return parameter;
    }

    public void setParameter(Object parameter) {
        this.parameter = parameter;
    }

    public Object getParameter1() {
        return parameter1;
    }

    public void setParameter1(Object parameter1) {
        this.parameter1 = parameter1;
    }
}
