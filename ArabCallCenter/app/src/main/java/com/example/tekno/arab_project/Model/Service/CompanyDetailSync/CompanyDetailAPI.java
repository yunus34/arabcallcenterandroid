package com.example.tekno.arab_project.Model.Service.CompanyDetailSync;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CompanyDetailAPI {
    @POST("Get")
    Call<CompanyDetailResponse> companyDetail(
            @Header("Authorization") String auth,
            @Query("Id") String companyId);
}
