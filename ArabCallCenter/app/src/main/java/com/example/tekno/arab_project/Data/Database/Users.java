package com.example.tekno.arab_project.Data.Database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Users")
public class Users extends BaseEntity{

    public static final String EMAIL = "Email";
    public static final String PASSWORD = "Password";
    public static final String NAME = "Name";
    public static final String SURNAME = "SurName";
    public static final String PHONE = "Phone";
    public static final String IS_BANNED = "IsBanned";

    @DatabaseField(columnName = EMAIL)
    private String userEmail;

    @DatabaseField(columnName = PASSWORD)
    private String userPassword;

    @DatabaseField(columnName = NAME)
    private String userName;

    @DatabaseField(columnName = SURNAME)
    private String userSurName;

    @DatabaseField(columnName = PHONE)
    private String userPhone;

    @DatabaseField(columnName = IS_BANNED)
    private Boolean userIsBanned;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurName() {
        return userSurName;
    }

    public void setUserSurName(String userSurName) {
        this.userSurName = userSurName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Boolean getUserIsBanned() {
        return userIsBanned;
    }

    public void setUserIsBanned(Boolean userIsBanned) {
        this.userIsBanned = userIsBanned;
    }
}
