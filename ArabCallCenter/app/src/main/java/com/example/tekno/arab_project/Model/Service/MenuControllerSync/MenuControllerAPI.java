package com.example.tekno.arab_project.Model.Service.MenuControllerSync;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MenuControllerAPI {
    @POST("Control")
    Call<MenuControllerResponse> menuController(
            @Header("Authorization") String auth,
            @Query("lastUpdateDate") String lastUpdateDate);
}
