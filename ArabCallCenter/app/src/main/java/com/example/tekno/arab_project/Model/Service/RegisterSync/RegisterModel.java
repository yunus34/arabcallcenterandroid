package com.example.tekno.arab_project.Model.Service.RegisterSync;

import android.util.Patterns;

public class RegisterModel implements RegisterValidation{
    private String Email;
    private String Password;
    private String GsmNo;
    private String Name;


    public RegisterModel(String email, String password, String gsmNo,String name) {
        this.Email = email;
        this.Password = password;
        this.GsmNo = gsmNo;
        this.Name=name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getGsmNo() {
        return GsmNo;
    }

    public void setGsmNo(String gsmNo) {
        GsmNo = gsmNo;
    }

    @Override
    public boolean isEmailValidData() {
        return  Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches();
    }

    @Override
    public boolean isPasswordValidData() {
        return getPassword().length()>5;
    }

    @Override
    public boolean isPhoneValidData() {
        return getGsmNo().length()==10;
    }
}
