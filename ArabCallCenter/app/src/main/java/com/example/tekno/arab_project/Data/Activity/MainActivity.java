package com.example.tekno.arab_project.Data.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Adapters.MenuItemAdapter;
import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Database.DB_Helper;
import com.example.tekno.arab_project.Data.Database.MenuList;
import com.example.tekno.arab_project.Data.Database.Parameters;
import com.example.tekno.arab_project.Data.Database.Querys.GeneralManager;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.MenuListModel;
import com.example.tekno.arab_project.Model.Service.MenuControllerSync.MenuControllerAPI;
import com.example.tekno.arab_project.Model.Service.MenuControllerSync.MenuControllerModel;
import com.example.tekno.arab_project.Model.Service.MenuControllerSync.MenuControllerResponse;
import com.example.tekno.arab_project.Model.Service.MenuControllerSync.MenuControllerRetrofitGET;
import com.example.tekno.arab_project.Model.Service.MenuListSync.MenuListAPI;
import com.example.tekno.arab_project.Model.Service.MenuListSync.MenuListResponse;
import com.example.tekno.arab_project.Model.Service.MenuListSync.MenuListResponseData;
import com.example.tekno.arab_project.Model.Service.MenuListSync.MenuListRetrofitGET;
import com.example.tekno.arab_project.Model.Service.RefreshTokenSync.RefreshTokenAPI;
import com.example.tekno.arab_project.Model.Service.RefreshTokenSync.RefreshTokenModel;
import com.example.tekno.arab_project.Model.Service.RefreshTokenSync.RefreshTokenResponse;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenAPI;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenModel;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenResponse;
import com.example.tekno.arab_project.Model.Service.TokenSync.TokenRetrofitGET;
import com.example.tekno.arab_project.R;
import com.google.android.gms.vision.barcode.Barcode;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = MainActivity.class.getName();
    private int CALL_PERMISSION_CODE = 101;
    private Toolbar toolbar;
    private List<MenuListModel> menuListModels;
    private List<MenuListResponseData> menuLists;
    private List<MenuList> menuListDB;
    private GridView gridView;
    private MenuItemAdapter menuItemAdapter;
    private BottomNavigationView bottomNavigationView;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private ProgressDialog progressDialog;

    private Integer BARCODE_READER_REQUEST_CODE = 401;
    private String access;
    private String refreshToken;

    private Util util;
    private DB_Helper db_helper;
    private GeneralManager generalManager;
    private Parameters parameters;
    private MenuList menuList;

    private SharedPreferences sharedPreferencesToken,sharedPreferencesUser;
    private SharedPreferences.Editor editorToken,editorUser;

    public boolean permission = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        util = new Util(this);
        db_helper = new DB_Helper(this);
        generalManager = new GeneralManager(db_helper);
        menuList = new MenuList();
        parameters = new Parameters();
        menuLists = new ArrayList<>();
        /*Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            menuLists = (List<MenuListResponseData>) bundle.getSerializable("MenuList");
        }*/
        SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");
        refreshToken = sharedPref.getString("refresh_token", "");

        gridView = (GridView) findViewById(R.id.menu);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigation);

        menuListModels = new ArrayList<>();
        //getToken();
        getMenuController();


        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.profile:
                        Intent intentProfile = new Intent(MainActivity.this, ProfileActivity.class);
                        startActivity(intentProfile);
                        break;
                    case R.id.language:
                        Intent intentLanguage = new Intent(MainActivity.this, LanguageActivity.class);
                        startActivity(intentLanguage);
                        break;
                    case R.id.logout:
                        sharedPreferencesToken = getApplicationContext().getSharedPreferences(Constants.ACCESS_TOKEN, 0);
                        editorToken = sharedPreferencesToken.edit();
                        editorToken.remove("access_token");
                        editorToken.commit();

                        sharedPreferencesToken = getApplicationContext().getSharedPreferences(Constants.USER_INFORMATIONS, 0);
                        SharedPreferences.Editor editorUserInformation = sharedPreferencesToken.edit();
                        editorUserInformation.remove("UserId");
                        editorUserInformation.remove("Name");
                        editorUserInformation.remove("Surname");
                        editorUserInformation.remove("Email");
                        editorUserInformation.remove("GsmNo");
                        editorUserInformation.remove("Password");
                        editorUserInformation.commit();

                        Intent intentTabLayout = new Intent(MainActivity.this, TabLayoutActivity.class);
                        startActivity(intentTabLayout);
                        finish();
                        break;
                    default:
                        return true;
                }
                return false;
            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.solution_center:
                        final Dialog dialog = new Dialog(MainActivity.this);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.dialog_soluation_center);
                        dialog.setCancelable(true);
                        final TextView soluation_center_text = (TextView) dialog.findViewById(R.id.arab_call_center_aoluation_center);
                        soluation_center_text.setTypeface(util.getTypeface());
                        final TextView soluation_center_number = (TextView) dialog.findViewById(R.id.soluation_center_number);
                        soluation_center_number.setTypeface(util.getTypeface());
                        Button dialogButton = (Button) dialog.findViewById(R.id.soluation_center_button);
                        dialogButton.setTypeface(util.getTypeface());

                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                permission = util.checkPermissionCall(MainActivity.this);
                                if (permission) {
                                    String number = soluation_center_number.getText().toString().trim();
                                    Intent intent = new Intent(Intent.ACTION_CALL);
                                    intent.setData(Uri.parse("tel:" + number));
                                    startActivity(intent);
                                    dialog.dismiss();
                                } else {
                                    String msg = getResources().getString(R.string.dialog_call_permission_message);
                                    // false Diaolg show on MainActivity
                                    util.getDialogInformation(MainActivity.this, msg, false);
                                }

                            }
                        });
                        dialog.show();
                        break;
                }
                return true;
            }
        });
    }

   /* private void getToken() {
        sharedPreferencesUser=getSharedPreferences(Constants.USER_INFORMATIONS, 0);
        TokenModel tokenModel = new TokenModel(username, password, Constants.GRANT_TYPE);
        TokenAPI tokenAPI = TokenRetrofitGET.getRetrofitToken().create(TokenAPI.class);
        Call<TokenResponse> call = tokenAPI.token(tokenModel.getUsername(), tokenModel.password, tokenModel.grant_type);
        call.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                TokenResponse tokenResponse = response.body();
                if (tokenResponse!= null) {
                    sharedPreferencesToken = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
                    editorToken = sharedPreferencesToken.edit();
                    editorToken.putString("access_token", tokenResponse.getAccessToken());
                    editorToken.putString("refresh_token", tokenResponse.getRefreshToken());
                    editorToken.commit();

                    access = Constants.TOKEN_TYPE + " " + tokenResponse.getAccessToken();

                } else {
                    Toasty.error(MainActivity.this, getResources().getText(R.string.validate_login_email_password), Toast.LENGTH_LONG).show();
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.e(TAG, "TokenResponse Error:" + t.toString());
            }
        });
    }*/

    private void getMenuController() {
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        Util.setProgressDialog(progressDialog);
        String date = null;

        try {
            if (generalManager.retrieveParameters().size() > 0) {
                date = generalManager.retrieveParameters().get(0).getName();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        MenuControllerModel menuControllerModel = new MenuControllerModel(date);
        MenuControllerAPI menuControllerAPI = MenuControllerRetrofitGET.getRetrofitMenuController().create(MenuControllerAPI.class);
        Call<MenuControllerResponse> call = menuControllerAPI.menuController(access, menuControllerModel.getLastUpdateDate());
        call.enqueue(new Callback<MenuControllerResponse>() {
            @Override
            public void onResponse(Call<MenuControllerResponse> call, Response<MenuControllerResponse> response) {
                try {
                    MenuControllerResponse menuControllerResponse = response.body();
                    if (menuControllerResponse != null) {
                        if (menuControllerResponse.getSuccess() && menuControllerResponse.getResponseData().getUpdateRequired()) {
                            parameters = generalManager.retrieveParameters().get(0);
                            parameters.setName(Util.getCurrentDateMilles());
                            parameters.setCreateDate(Util.getCurrentDate());
                            parameters.setDeleted(false);
                            parameters.setUpdateDate(Util.getCurrentDate());
                            parameters.setDescription("first");
                            parameters.setGroupName("MenuController");
                            //db_helper.createOrUpdate(parameters);
                            db_helper.getDao(Parameters.class).update(parameters);
                        }

                        getMenuList(menuControllerResponse.getResponseData().getUpdateRequired());

                    } else {
                        //refreshToken();
                        //getMenuController();
                        Util.dismisProgressDialog(progressDialog);
                    }
                } catch (SQLException e) {
                    Log.e(TAG, "SQL Update Parameters Error:" + e);
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<MenuControllerResponse> call, Throwable t) {
                Log.e(TAG, "MenuControllerResponse Error:" + t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }

    private void getMenuList(Boolean updateRequired) {
        //if (updateRequired) {
        MenuListAPI menuListAPI = MenuListRetrofitGET.getRetrofitMenuList().create(MenuListAPI.class);
        Call<MenuListResponse> call = menuListAPI.menuList(access);
        call.enqueue(new Callback<MenuListResponse>() {
            @Override
            public void onResponse(Call<MenuListResponse> call, Response<MenuListResponse> response) {
                MenuListResponse menuListResponse = response.body();
                menuLists = menuListResponse.getResponseData();
                if (menuListResponse != null) {
                    try {
                        for (int i = 0; i < menuListResponse.getResponseData().size(); i++) {
                            menuList.setId(UUID.fromString(menuListResponse.getResponseData().get(i).getId()));
                            menuList.setCreateDate(Util.getCurrentDate());
                            menuList.setUpdateDate(Util.getCurrentDate());
                            menuList.setDeleted(false);
                            menuList.setName(menuListResponse.getResponseData().get(i).getName());
                            menuList.setCreateUser(String.valueOf(menuListResponse.getResponseData().get(i).getCreateUser()));
                            menuList.setSequence(menuListResponse.getResponseData().get(i).getSequence());
                            menuList.setUpdateUser(String.valueOf(menuListResponse.getResponseData().get(i).getUpdateUser()));
                            menuList.setIconPath(menuListResponse.getResponseData().get(i).getIconPath());
                            if (generalManager.retrieveMenuListsById(UUID.fromString(menuListResponse.getResponseData().get(i).getId())).size() > 0) {
                                db_helper.getDao(MenuList.class).update(menuList);
                            } else {
                                db_helper.getDao(MenuList.class).create(menuList);
                            }
                        }
                        listFill(menuLists);
                        //new GetMenuLists().execute();

                    } catch (SQLException e) {
                        Log.e(TAG, "MenuListResponse SQLException:" + e.toString());
                        Util.dismisProgressDialog(progressDialog);
                    }
                }
            }

            @Override
            public void onFailure(Call<MenuListResponse> call, Throwable t) {
                Log.e(TAG, "MenuListResponse Error:" + t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
        /*} else {
            new GetMenuLists().execute();
        }*/
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    private void listFill(List<MenuListResponseData> menuListResponseData) {
        Bitmap icon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_vip);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        icon.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        for (MenuListResponseData menuListRespons : menuListResponseData) {
            menuListModels.add(new MenuListModel(menuListRespons.getName(),
                    menuListRespons.getIconPath(), menuListRespons.getSequence()));
        }
        //menuListModels.add(new MenuListModel("VIP", encodedImage,1));
        menuItemAdapter = new MenuItemAdapter(this, menuListModels);
        gridView.setAdapter(menuItemAdapter);
        gridView.setOnItemClickListener(this);
        Util.dismisProgressDialog(progressDialog);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
       /* if (i == 0) {
            Intent intent = new Intent(MainActivity.this, BarcodeScanActivity.class);
            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
        }else{*/
        companyList(i);
        // }

        /*for(int j=0;j<menuLists.size();j++){
            if (menuLists.get(i).getSequence() == i) {
                companyList(i);
                Intent intent = new Intent(MainActivity.this, CompanyListActivity.class);
                startActivity(intent);
            }
        }*/
    }

    private void companyList(int position) {

        /*Intent intent = new Intent(MainActivity.this, CompanyListActivity.class);
        intent.putExtra("menuListId", menuListDB.get(position).getId().toString());
        intent.putExtra("menuListName", menuListDB.get(position).getName());
        startActivity(intent);*/
        Intent intent = new Intent(MainActivity.this, CompanyListActivity.class);
        intent.putExtra("menuListId", menuLists.get(position).getId());
        intent.putExtra("menuListName", menuLists.get(position).getName());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
               // final Barcode barcode = data.getParcelableExtra("barcode");
               // Toast.makeText(this, barcode.displayValue, Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*public void checkPermissionCall() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            permission = true;
        }
    }

    public void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSION_CODE);
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CALL_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                permission = true;
            } else {
                permission = false;
            }
        }
    }

    public class GetMenuLists extends AsyncTask<Void, Void, List<MenuListModel>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<MenuListModel> doInBackground(Void... voids) {
            try {
                menuListDB = generalManager.retrieveMenuLists();
                for (MenuList menuList : menuListDB) {
                    menuListModels.add(new MenuListModel(menuList.getName(),
                            menuList.getIconPath(), menuList.getSequence()));
                }
            } catch (SQLException e) {
                e.printStackTrace();
                Util.dismisProgressDialog(progressDialog);
            }
            return menuListModels;
        }

        @Override
        protected void onPostExecute(List<MenuListModel> menuListModels) {
            super.onPostExecute(menuListModels);
            //listFill(menuListModels);
            Util.dismisProgressDialog(progressDialog);
        }
    }

    public void refreshToken() {
        final SharedPreferences sharedPreferencesToken = this.getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        final SharedPreferences.Editor editorToken = sharedPreferencesToken.edit();
        RefreshTokenModel refreshTokenModel = new RefreshTokenModel(Constants.GRANT_TYPE_REFRESH_TOKEN, refreshToken);
        RefreshTokenAPI tokenAPI = TokenRetrofitGET.getRetrofitToken().create(RefreshTokenAPI.class);
        Call<RefreshTokenResponse> call = tokenAPI.refreshToken(refreshTokenModel.getGrant_type(), refreshTokenModel.getRefresh_token());
        call.enqueue(new Callback<RefreshTokenResponse>() {
            @Override
            public void onResponse(Call<RefreshTokenResponse> call, Response<RefreshTokenResponse> response) {
                RefreshTokenResponse refreshTokenResponse = response.body();
                if (refreshTokenResponse != null) {
                    editorToken.putString("access_token", refreshTokenResponse.getAccessToken());
                    editorToken.putString("refresh_token", refreshTokenResponse.getRefreshToken());
                    editorToken.commit();
                } else {
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<RefreshTokenResponse> call, Throwable t) {
                Log.e(TAG, "RefreshTokenResponse Error;" + t);
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }

}
