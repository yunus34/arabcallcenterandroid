package com.example.tekno.arab_project.Model.Service.AssessmentListSync;

public class AssessmentListModel {
    private String ModelId;
    private Integer Skip;
    private Integer Take;

    public AssessmentListModel(String modelId, Integer skip, Integer take) {
        ModelId = modelId;
        Skip = skip;
        Take = take;
    }

    public String getModelId() {
        return ModelId;
    }

    public void setModelId(String modelId) {
        ModelId = modelId;
    }

    public Integer getSkip() {
        return Skip;
    }

    public void setSkip(Integer skip) {
        Skip = skip;
    }

    public Integer getTake() {
        return Take;
    }

    public void setTake(Integer take) {
        Take = take;
    }
}
