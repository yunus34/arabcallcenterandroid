package com.example.tekno.arab_project.Model.Service.LoginSync;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginAPI {
    @POST("Login")
    Observable<LoginResponse> login(
            @Header("Authorization") String auth,
            @Body LoginModel loginModel);
}
