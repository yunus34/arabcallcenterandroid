package com.example.tekno.arab_project.Model.Service.CompanyDetailSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyDetailResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("responseName")
    @Expose
    private String responseName;
    @SerializedName("responseData")
    @Expose
    private CompanyDetailResponseData companyDetailResponseData;
    @SerializedName("error")
    @Expose
    private Object error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public CompanyDetailResponseData getCompanyDetailResponseData() {
        return companyDetailResponseData;
    }

    public void setCompanyDetailResponseData(CompanyDetailResponseData companyDetailResponseData) {
        this.companyDetailResponseData = companyDetailResponseData;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}
