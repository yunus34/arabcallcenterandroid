package com.example.tekno.arab_project.Model.Service.UserUpdatePassword;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserUpdatePasswordRetrofitGET {
    private static Retrofit retrofit = null;
    private static String BASE_URL = Constants.User_Update_Password_URL;

    public static Retrofit getRetrofitUserUpdatePassword() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(Util.getTimeOut())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }
        return retrofit;
    }
}
