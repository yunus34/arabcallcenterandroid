package com.example.tekno.arab_project.Model.Service.RegisterSync;

public interface RegisterValidation {
    boolean isEmailValidData();
    boolean isPasswordValidData();
    boolean isPhoneValidData();
}
