package com.example.tekno.arab_project.Model.CompanyList;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.tekno.arab_project.Data.Activity.CompanyDetailActivity;
import com.example.tekno.arab_project.Data.Enum.GalleryTypeId;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListGalleryResponse;
import com.example.tekno.arab_project.Model.Service.CompanyListSync.CompanyListInformationResponse;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

public class ImageSliderAdapter extends PagerAdapter {

    private Context context;
    private List<CompanyListGalleryResponse> companyListGalleryResponse;
    private CompanyListInformationResponse companyListInformationResponse;
    private ProgressBar progressBar;
    private int itemSize=0;

    public ImageSliderAdapter(Context context, List<CompanyListGalleryResponse> companyListGalleryResponse, CompanyListInformationResponse companyListInformationResponse,ProgressBar progressBar) {
        this.context = context;
        this.companyListGalleryResponse = companyListGalleryResponse;
        this.companyListInformationResponse = companyListInformationResponse;
        this.progressBar=progressBar;
        for(CompanyListGalleryResponse listGalleryResponse:companyListGalleryResponse){
            if(listGalleryResponse.getGalleryTypeId()== GalleryTypeId.Image.getValue()){
                this.itemSize++;
            }
        }
    }

    @Override
    public int getCount() {
        return itemSize;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView=new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        if(companyListGalleryResponse.get(position).getGalleryTypeId()== GalleryTypeId.Image.getValue()){
            Glide.with(context)
                    .load(companyListGalleryResponse.get(position).getGalleryPath())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            //progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            //progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
            container.addView(imageView,0);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDetail = new Intent(context, CompanyDetailActivity.class);
                intentDetail.putExtra("companyDetail", (Serializable) companyListInformationResponse);
                context.startActivity(intentDetail);
            }
        });

        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }
}
