package com.example.tekno.arab_project.View;

import java.sql.SQLException;

public interface RegisterView {
    void registerSuccess();
    void registerError(Integer valid);
    void registerDatabaseError(SQLException e);
}
