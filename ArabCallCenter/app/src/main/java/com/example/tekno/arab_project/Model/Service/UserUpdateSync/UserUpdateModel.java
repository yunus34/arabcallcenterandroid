package com.example.tekno.arab_project.Model.Service.UserUpdateSync;


public class UserUpdateModel {
    private String Id;
    private String Email;
    private String Name;
    private String Surname;
    private String GsmNo;

    public UserUpdateModel(String id, String email, String name, String surname, String gsmNo) {
        this.Id = id;
        this.Email = email;
        this.Name = name;
        this.Surname = surname;
        this.GsmNo = gsmNo;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getGsmNo() {
        return GsmNo;
    }

    public void setGsmNo(String gsmNo) {
        GsmNo = gsmNo;
    }
}
