package com.example.tekno.arab_project.View;

import com.example.tekno.arab_project.Data.Database.MenuList;
import com.example.tekno.arab_project.Model.MenuListModel;
import com.example.tekno.arab_project.Model.Service.MenuListSync.MenuListResponseData;

import java.sql.SQLException;
import java.util.List;

public interface LoginView {
    void loginSuccess();
    void registerSuccess();
    void loginError();
    void loginError(Throwable e);
    void loginError(Integer valid);
    void loginError(NullPointerException e);
    void registerDatabaseError(SQLException e);
}
