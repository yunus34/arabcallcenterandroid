package com.example.tekno.arab_project.Model.Service.CompanyDetailSync;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompanyDetailResponseData{
    @SerializedName("CompanyInformation")
    @Expose
    private CompanyDetailInformationResponse companyDetailInformationResponse;
    @SerializedName("CompanyGalleries")
    @Expose
    private List<CompanyDetailGalleryResponse> companyGalleries = null;
    @SerializedName("CompanyAssessments")
    @Expose
    private List<CompanyDetailAssessmentResponse> companyAssessments = null;

    public CompanyDetailInformationResponse getCompanyDetailInformationResponse() {
        return companyDetailInformationResponse;
    }

    public void setCompanyDetailInformationResponse(CompanyDetailInformationResponse companyDetailInformationResponse) {
        this.companyDetailInformationResponse = companyDetailInformationResponse;
    }

    public List<CompanyDetailGalleryResponse> getCompanyGalleries() {
        return companyGalleries;
    }

    public void setCompanyGalleries(List<CompanyDetailGalleryResponse> companyGalleries) {
        this.companyGalleries = companyGalleries;
    }

    public List<CompanyDetailAssessmentResponse> getCompanyAssessments() {
        return companyAssessments;
    }

    public void setCompanyAssessments(List<CompanyDetailAssessmentResponse> companyAssessments) {
        this.companyAssessments = companyAssessments;
    }
}
