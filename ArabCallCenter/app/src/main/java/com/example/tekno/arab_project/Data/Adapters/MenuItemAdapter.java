package com.example.tekno.arab_project.Data.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.MenuListModel;
import com.example.tekno.arab_project.R;

import java.util.List;

public class MenuItemAdapter extends BaseAdapter {
    private Activity activity;
    private List<MenuListModel> menuListModel;
    private Util util;
    public MenuItemAdapter(Activity activity, List<MenuListModel> menuListModel) {
        this.activity = activity;
        this.menuListModel = menuListModel;
        this.util=new Util(activity);
    }

    @Override
    public int getCount() {
        return menuListModel.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        convertView = LayoutInflater.from(activity).inflate(R.layout.item_list_menu, null);
        TextView txtMenu = (TextView) convertView.findViewById(R.id.menu_names);
        txtMenu.setTypeface(util.getTypeface());
        ImageView imgMenu = (ImageView) convertView.findViewById(R.id.menu_icons);
        MenuListModel menuListMdl = menuListModel.get(position);
        txtMenu.setText(menuListMdl.getName());
        Glide.with(activity).load(menuListMdl.getImage()).into(imgMenu);
        return convertView;
    }
}
