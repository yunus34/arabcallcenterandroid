package com.example.tekno.arab_project.Model.Service.AssessmentInsertSync;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Util;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AssesmentInsertRestrofitGET {
    private static Retrofit retrofit=null;
    private static String BASE_URL= Constants.Assessment_Insert_URL;
    public static Retrofit getRetrofitAssessmentInsert()
    {
        if(retrofit==null){
            retrofit=new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(Util.getTimeOut())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }
        return retrofit;
    }
}

