package com.example.tekno.arab_project.Model.Service.RegisterSync;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Util;

import retrofit2.Retrofit;

import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterRetrofitGET {
    private static Retrofit retrofit = null;
    private static String Base_Url = Constants.Register_URL;

    public static Retrofit getRetrofit() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Base_Url)
                    .client(Util.getTimeOut())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }
        return retrofit;
    }
}
