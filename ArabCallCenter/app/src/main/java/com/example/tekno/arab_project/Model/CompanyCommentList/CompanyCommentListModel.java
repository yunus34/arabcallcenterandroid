package com.example.tekno.arab_project.Model.CompanyCommentList;

public class CompanyCommentListModel {
    private String commentDate;
    private Integer commentRate;
    private String commentText;


    public CompanyCommentListModel(String commentDate, Integer commentRate, String commentText) {
        this.commentDate = commentDate;
        this.commentRate = commentRate;
        this.commentText = commentText;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public Integer getCommentRate() {
        return commentRate;
    }

    public void setCommentRate(Integer commentRate) {
        this.commentRate = commentRate;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
}
