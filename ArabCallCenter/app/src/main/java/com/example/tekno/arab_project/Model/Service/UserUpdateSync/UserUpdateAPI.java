package com.example.tekno.arab_project.Model.Service.UserUpdateSync;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserUpdateAPI {
    @POST("Update")
    Call<UserUpdateResponse> userUpdate(
            @Header("Authorization") String auth,
            @Body UserUpdateModel updateModel);
}
