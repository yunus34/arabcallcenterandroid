package com.example.tekno.arab_project.Model.Service.RegisterSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("responseName")
    @Expose
    private String responseName;
    @SerializedName("responseData")
    @Expose
    private RegisterResponseUserData registerResponseUserData;
    @SerializedName("error")
    @Expose
    private RegisterResponseError error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public RegisterResponseUserData getRegisterResponseUserData() {
        return registerResponseUserData;
    }

    public void setRegisterResponseUserData(RegisterResponseUserData registerResponseUserData) {
        this.registerResponseUserData = registerResponseUserData;
    }

    public RegisterResponseError getError() {
        return error;
    }

    public void setError(RegisterResponseError error) {
        this.error = error;
    }
}
