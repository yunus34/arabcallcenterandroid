package com.example.tekno.arab_project.Data.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Database.DB_Helper;
import com.example.tekno.arab_project.Data.Database.Querys.GeneralManager;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.Service.UserUpdatePassword.UserUpdatePasswordAPI;
import com.example.tekno.arab_project.Model.Service.UserUpdatePassword.UserUpdatePasswordModel;
import com.example.tekno.arab_project.Model.Service.UserUpdatePassword.UserUpdatePasswordResponse;
import com.example.tekno.arab_project.Model.Service.UserUpdatePassword.UserUpdatePasswordRetrofitGET;
import com.example.tekno.arab_project.Model.Service.UserUpdateSync.UserUpdateAPI;
import com.example.tekno.arab_project.Model.Service.UserUpdateSync.UserUpdateModel;
import com.example.tekno.arab_project.Model.Service.UserUpdateSync.UserUpdateResponse;
import com.example.tekno.arab_project.Model.Service.UserUpdateSync.UserUpdateRetrofitGET;
import com.example.tekno.arab_project.R;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = ProfileActivity.class.getName();
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences, sharedPreferencesUserInformation, sharedPreferencesPassword;
    private SharedPreferences.Editor editorUserInformation, editorPassword;
    private UserUpdateResponse userUpdateResponse;
    private UserUpdatePasswordResponse userUpdatePasswordResponse;
    private Util util;
    private DB_Helper db_helper;
    private GeneralManager generalManager;
    private TextView nameText;
    private TextView surNameText;
    private TextView emailText;
    private TextView passwordText;
    private TextView gsmNoText;
    private Button profile;
    private ImageView passwordUpdate;

    private EditText name;
    private EditText surName;
    private EditText email;
    private EditText gsmNo;

    private Dialog dialog;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.profile_toolbar));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        db_helper = new DB_Helper(this);
        generalManager = new GeneralManager(db_helper);
        dialog = new Dialog(ProfileActivity.this);
        progressDialog = new ProgressDialog(this);

        nameText = (TextView) findViewById(R.id.nameText);
        nameText.setTypeface(util.getTypefaceBold());
        surNameText = (TextView) findViewById(R.id.surNameText);
        surNameText.setTypeface(util.getTypefaceBold());
        emailText = (TextView) findViewById(R.id.emailText);
        emailText.setTypeface(util.getTypefaceBold());
        passwordText = (TextView) findViewById(R.id.passwordText);
        passwordText.setTypeface(util.getTypefaceBold());
        gsmNoText = (TextView) findViewById(R.id.gsmNoText);
        gsmNoText.setTypeface(util.getTypefaceBold());
        profile = (Button) findViewById(R.id.profileApply);
        profile.setTypeface(util.getTypeface());
        passwordUpdate = (ImageView) findViewById(R.id.password_update);
        name = (EditText) findViewById(R.id.name);
        name.setTypeface(util.getTypeface());
        surName = (EditText) findViewById(R.id.surName);
        surName.setTypeface(util.getTypeface());
        email = (EditText) findViewById(R.id.email);
        email.setTypeface(util.getTypeface());
        gsmNo = (EditText) findViewById(R.id.gsmNo);
        gsmNo.setTypeface(util.getTypeface());

        sharedPreferences = getSharedPreferences(Constants.USER_INFORMATIONS, 0);
        name.setText(sharedPreferences.getString("Name", ""));
        surName.setText(sharedPreferences.getString("Surname", ""));
        email.setText(sharedPreferences.getString("Email", ""));
        gsmNo.setText(sharedPreferences.getString("GsmNo", ""));

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isEmailValidData(email.getText().toString().trim())) {
                    updateData();
                } else {
                    Toasty.error(ProfileActivity.this, getResources().getText(R.string.validate_email), Toast.LENGTH_LONG).show();
                }
            }
        });
        passwordUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setContentView(R.layout.dialog_password_update);
                dialog.setCancelable(true);

                final TextView currentPasswordText = (TextView) dialog.findViewById(R.id.currentPasswordText);
                currentPasswordText.setTypeface(util.getTypefaceBold());
                final TextView newPasswordText = (TextView) dialog.findViewById(R.id.newPasswordText);
                newPasswordText.setTypeface(util.getTypefaceBold());
                final EditText current_password = (EditText) dialog.findViewById(R.id.current_password);
                current_password.setTypeface(util.getTypeface());
                final EditText new_password = (EditText) dialog.findViewById(R.id.new_password);
                new_password.setTypeface(util.getTypeface());

                Button dialogButton = (Button) dialog.findViewById(R.id.btn_pw_update);
                dialogButton.setTypeface(util.getTypeface());

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String userId = sharedPreferences.getString("UserId", "");
                        String currentPassword = current_password.getText().toString().trim();
                        String newPassword = new_password.getText().toString().trim();
                        UserUpdatePasswordModel userUpdatePasswordModel = new UserUpdatePasswordModel(userId, currentPassword, newPassword);
                        boolean isPasswordSuccess = userUpdatePasswordModel.isPasswordValidData();
                        if (currentPassword.equals("")) {
                            Toasty.error(ProfileActivity.this, getResources().getText(R.string.validate_user_update_password_empty), Toast.LENGTH_LONG).show();
                        } else if (newPassword.equals("")) {
                            Toasty.error(ProfileActivity.this, getResources().getText(R.string.validate_user_update_password_empty), Toast.LENGTH_LONG).show();
                        } else if(!isPasswordSuccess) {
                            Toasty.error(ProfileActivity.this,getResources().getText(R.string.validate_register_password), Toast.LENGTH_SHORT).show();
                        }else{
                            updateDataPassword(userUpdatePasswordModel);
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    private void updateDataPassword(UserUpdatePasswordModel userUpdatePasswordModel) {
        progressDialog.show();
        Util.setProgressDialog(progressDialog);

        SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        String access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");
        UserUpdatePasswordAPI userUpdatePasswordAPI = UserUpdatePasswordRetrofitGET.getRetrofitUserUpdatePassword().create(UserUpdatePasswordAPI.class);
        Call<UserUpdatePasswordResponse> call = userUpdatePasswordAPI.userUpdatePassword(access, userUpdatePasswordModel);
        call.enqueue(new Callback<UserUpdatePasswordResponse>() {
            @Override
            public void onResponse(Call<UserUpdatePasswordResponse> call, Response<UserUpdatePasswordResponse> response) {
                userUpdatePasswordResponse = response.body();
                if (userUpdatePasswordResponse != null && userUpdatePasswordResponse.getSuccess()) {
                    sharedPreferencesPassword = getSharedPreferences(Constants.USER_INFORMATIONS, 0);
                    editorPassword = sharedPreferencesPassword.edit();
                    editorPassword.putString("Password", userUpdatePasswordResponse.getResponseData().getPassword());
                    editorPassword.commit();

                    Toasty.success(ProfileActivity.this, getResources().getText(R.string.user_update), Toast.LENGTH_LONG).show();
                    Util.dismisProgressDialog(progressDialog);
                    dialog.dismiss();
                } else {
                    Toasty.error(ProfileActivity.this, getResources().getText(R.string.user_update_password_error), Toast.LENGTH_LONG).show();
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<UserUpdatePasswordResponse> call, Throwable t) {
                Log.e(TAG, "UserUpdatePasswordResponse" + t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });

    }

    private void updateData() {

        progressDialog.show();
        Util.setProgressDialog(progressDialog);

        String userId = sharedPreferences.getString("UserId", "");
        String userName = name.getText().toString().trim();
        String userSurname = surName.getText().toString().trim();
        String userEmail = email.getText().toString().trim();
        String userPhone = gsmNo.getText().toString().trim();

        SharedPreferences sharedPref = getSharedPreferences(Constants.ACCESS_TOKEN, 0);
        String access = Constants.TOKEN_TYPE + " " + sharedPref.getString("access_token", "");
        UserUpdateModel userUpdateModel = new UserUpdateModel(userId, userEmail, userName, userSurname, userPhone);

        UserUpdateAPI userUpdateAPI = UserUpdateRetrofitGET.getRetrofitUserUpdate().create(UserUpdateAPI.class);
        Call<UserUpdateResponse> call = userUpdateAPI.userUpdate(access, userUpdateModel);
        call.enqueue(new Callback<UserUpdateResponse>() {
            @Override
            public void onResponse(Call<UserUpdateResponse> call, Response<UserUpdateResponse> response) {
                userUpdateResponse = response.body();

                if (userUpdateResponse != null && userUpdateResponse.getSuccess()) {

                    sharedPreferencesUserInformation = getSharedPreferences(Constants.USER_INFORMATIONS, 0);
                    editorUserInformation = sharedPreferencesUserInformation.edit();
                    editorUserInformation.putString("UserId", String.valueOf(userUpdateResponse.getResponseData().getId()));
                    editorUserInformation.putString("Name", userUpdateResponse.getResponseData().getName());
                    editorUserInformation.putString("Surname", userUpdateResponse.getResponseData().getSurname());
                    editorUserInformation.putString("Email", userUpdateResponse.getResponseData().getEmail());
                    editorUserInformation.putString("GsmNo", userUpdateResponse.getResponseData().getGsmNo());
                    editorUserInformation.commit();
                    Util.dismisProgressDialog(progressDialog);
                    Toasty.success(ProfileActivity.this, getResources().getText(R.string.user_update), Toast.LENGTH_LONG).show();
                } else {
                    Toasty.error(ProfileActivity.this, getResources().getText(R.string.user_update_error), Toast.LENGTH_LONG).show();
                    Util.dismisProgressDialog(progressDialog);
                }
            }

            @Override
            public void onFailure(Call<UserUpdateResponse> call, Throwable t) {
                Log.e(TAG, "UserUpdateResponse Error:" + t.toString());
                Util.dismisProgressDialog(progressDialog);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
