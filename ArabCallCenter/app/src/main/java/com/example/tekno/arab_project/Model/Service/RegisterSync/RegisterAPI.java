package com.example.tekno.arab_project.Model.Service.RegisterSync;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface RegisterAPI {

    @POST("Register")
    Observable<RegisterResponse> register(
            @Header("Application-Signature") String auth,
            @Body RegisterModel registerModel);
}
