package com.example.tekno.arab_project.Model.Service.UserUpdatePassword;

public class UserUpdatePasswordModel implements UpdatePasswordValidation {
    private String userId;
    private String currentPassword;
    private String newPassword;

    public UserUpdatePasswordModel(String userId, String currentPassword, String newPassword) {
        this.userId = userId;
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public boolean isPasswordValidData() {
        return getNewPassword().length()>5;
    }
}
