package com.example.tekno.arab_project.Model.Service.FeatureListSync;

import io.reactivex.Observable;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface FeatureListAPI {
    @POST("List")
    Observable<FeatureListResponse> featureList(
            @Header("Authorization") String auth);
}
