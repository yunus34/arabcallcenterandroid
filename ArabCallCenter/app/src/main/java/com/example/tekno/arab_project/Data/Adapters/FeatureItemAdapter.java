package com.example.tekno.arab_project.Data.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Model.FeatureListModel;
import com.example.tekno.arab_project.R;

import java.util.List;

public class FeatureItemAdapter extends BaseAdapter {

    private Activity activity;
    private List<FeatureListModel> featureListModels;
    private Util util;

    public FeatureItemAdapter(Activity activity, List<FeatureListModel> featureListModels) {
        this.activity = activity;
        this.featureListModels = featureListModels;
        this.util=new Util(activity);
    }

    @Override
    public int getCount() {
        return featureListModels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        convertView = LayoutInflater.from(activity).inflate(R.layout.item_list_feature, null);
        TextView txtFeature = (TextView) convertView.findViewById(R.id.fatureText);
        txtFeature.setTypeface(util.getTypeface());
        ImageView imgFeature = (ImageView) convertView.findViewById(R.id.fatureImage);
        FeatureListModel featureListModel = featureListModels.get(position);
        txtFeature.setText(featureListModel.getName());
        Glide.with(activity).load(featureListModel.getImage()).into(imgFeature);
        return convertView;
    }
}
