package com.example.tekno.arab_project.Presenter;

import android.app.Activity;

public interface IRegisterPresenter {
    void performRegister(Activity activity, String userMail, String gsmNo, String userPassword);
}
