package com.example.tekno.arab_project.Model.Service.RefreshTokenSync;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RefreshTokenAPI {
    @FormUrlEncoded
    @POST("token")
    Call<RefreshTokenResponse> refreshToken(
            @Field("grant_type") String grant_type,
            @Field("refresh_token") String refresh_token
    );

}
