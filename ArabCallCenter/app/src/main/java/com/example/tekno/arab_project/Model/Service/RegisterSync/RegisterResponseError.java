package com.example.tekno.arab_project.Model.Service.RegisterSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponseError {
    @SerializedName("errorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("errorInnerException")
    @Expose
    private Object errorInnerException;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getErrorInnerException() {
        return errorInnerException;
    }

    public void setErrorInnerException(Object errorInnerException) {
        this.errorInnerException = errorInnerException;
    }
}
