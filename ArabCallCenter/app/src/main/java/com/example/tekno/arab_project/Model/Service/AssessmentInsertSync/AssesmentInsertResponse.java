package com.example.tekno.arab_project.Model.Service.AssessmentInsertSync;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssesmentInsertResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("responseName")
    @Expose
    private String responseName;
    @SerializedName("responseData")
    @Expose
    private AssessmentInsertResponseData assessmentInsertResponseData;
    @SerializedName("error")
    @Expose
    private Object error;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public AssessmentInsertResponseData getAssessmentInsertResponseData() {
        return assessmentInsertResponseData;
    }

    public void setAssessmentInsertResponseData(AssessmentInsertResponseData assessmentInsertResponseData) {
        this.assessmentInsertResponseData = assessmentInsertResponseData;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }
}
