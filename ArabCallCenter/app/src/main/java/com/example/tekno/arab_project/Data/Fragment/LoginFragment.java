package com.example.tekno.arab_project.Data.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekno.arab_project.Data.Activity.MainActivity;
import com.example.tekno.arab_project.Data.Constants;
import com.example.tekno.arab_project.Data.Enum.RegisterDataValidation;
import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.Presenter.LoginPresenter;
import com.example.tekno.arab_project.Presenter.RegisterPresenter;
import com.example.tekno.arab_project.R;
import com.example.tekno.arab_project.View.LoginView;
import com.example.tekno.arab_project.View.RegisterView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.sql.SQLException;

import es.dmoral.toasty.Toasty;

public class LoginFragment extends Fragment implements LoginView{
    private static final String TAG = LoginFragment.class.getName();
    private SignInButton googleSignInButton;
    private GoogleSignInClient googleSignInClient;
    private GoogleSignInAccount account;
    private EditText email;
    private EditText password;
    private TextView login;
    private TextView forgotPassword;
    private CheckBox rememberMe;
    private LoginPresenter loginPresenter;
    private RegisterPresenter registerPresenter;
    private RelativeLayout progress_layout;
    private Util util;
    private String registerResponseEmail;
    private String registerResponsePassword;
    private boolean remember_me = false;
    private Integer google_request_code = 1022;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        util = new Util(getActivity());
        loginPresenter = new LoginPresenter(this);
        registerPresenter = new RegisterPresenter(this, true);
        email = (EditText) view.findViewById(R.id.email);
        password = (EditText) view.findViewById(R.id.password);
        rememberMe = (CheckBox) view.findViewById(R.id.rememberMe);
        forgotPassword = (TextView) view.findViewById(R.id.forgotPassword);
        progress_layout = (RelativeLayout) view.findViewById(R.id.progress_layout);
        googleSignInButton = view.findViewById(R.id.sign_in_google);

        Typeface newTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Light.ttf");
        email.setTypeface(util.getTypeface());
        password.setTypeface(newTypeface);
        rememberMe.setTypeface(newTypeface);
        forgotPassword.setTypeface(newTypeface);

        SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.USER_REMEMBER_ME, 0);
        if (!sharedPref.getString("Email", "").equals("") && !sharedPref.getString("Password", "").equals("")) {
            rememberMe.setChecked(true);
            email.setText(sharedPref.getString("Email", ""));
            password.setText(sharedPref.getString("Password", ""));
        }
        login = (TextView) view.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Util.isEmpty((EditText) email)) {
                    email.requestFocus();
                    email.setError(getResources().getText(R.string.validate_email_empty));
                } else if (Util.isEmpty(password)) {
                    password.requestFocus();
                    password.setError(getResources().getText(R.string.validate_pw_empty));
                } else {

                    if (rememberMe.isChecked()) {
                        remember_me = true;
                    }
                    loginPresenter.performLogin(getActivity(), email.getText().toString().trim(), password.getText().toString().trim(), "", remember_me, false);
                }
            }
        });

        //Google sign-in
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        googleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, google_request_code);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case 1022:
                    try {
                        // The Task returned from this call is always completed, no need to attach
                        // a listener.
                        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                        account = task.getResult(ApiException.class);
                        onLoggedIn(account);
                    } catch (ApiException e) {
                        // The ApiException status code indicates the detailed failure reason.
                        Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
                    }
                    break;
            }
    }

    private void onLoggedIn(GoogleSignInAccount account) {
        loginPresenter.performLogin(getActivity(), account.getEmail(), Constants.googlePassword, account.getDisplayName(), remember_me, true);
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        Toasty.success(getActivity(), getResources().getText(R.string.validate_login_success), Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void loginError() {
        Toasty.error(getActivity(), getResources().getText(R.string.validate_email), Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginError(Throwable e) {
        Toasty.error(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void loginError(Integer valid) {
        if (valid == RegisterDataValidation.UserRequired.getValid() && account != null) {
            loginPresenter.performLogin(getActivity(), account.getEmail(), Constants.googlePassword, account.getDisplayName(), true, false);
        }
    }

    @Override
    public void loginError(NullPointerException e) {
        Toasty.error(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
    }

    public void setRegisterResponseEmail(String registerResponseEmail) {
        this.registerResponseEmail = registerResponseEmail;
    }

    public void setRegisterResponsePassword(String registerResponsePassword) {
        this.registerResponsePassword = registerResponsePassword;
    }

    @Override
    public void registerSuccess() {
        if (account != null)
            loginPresenter.performLogin(getActivity(), account.getEmail(), Constants.googlePassword, account.getDisplayName(), true, false);
    }

    @Override
    public void registerDatabaseError(SQLException e) {

    }
}
