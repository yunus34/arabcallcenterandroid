package com.example.tekno.arab_project.Data.Activity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.tekno.arab_project.R;
import com.google.android.gms.vision.text.Element;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideoViewActivity extends YouTubeBaseActivity {
    private YouTubePlayerView videoView;
    private Toolbar toolbar;
    private YouTubePlayer.OnInitializedListener onInitializedListener;
    private String YOUTUBE_API_KEY="AIzaSyBWMdJn88BqZ3G7zECleSKe7lPR-8Xv5nA";
   //
   private String videoUrl;
    private String videoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            videoUrl=bundle.getString("VideoURL");
        }
        videoPath=videoUrl.substring(30);
        videoView = (YouTubePlayerView) findViewById(R.id.youtubeVideoView);
        onInitializedListener=new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(videoPath);
                youTubePlayer.cueVideo(videoPath);


            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        };
        videoView.initialize(YOUTUBE_API_KEY,onInitializedListener);


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
