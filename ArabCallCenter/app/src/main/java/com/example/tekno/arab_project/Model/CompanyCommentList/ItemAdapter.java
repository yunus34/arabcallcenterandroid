package com.example.tekno.arab_project.Model.CompanyCommentList;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tekno.arab_project.Data.Activity.CompanyCommentList;
import com.example.tekno.arab_project.Data.ILoadMore;
import com.example.tekno.arab_project.R;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<CompanyCommentListModel> datas;
    private final int VIEW_TYPE_ITEM=0;
    private final int VIEW_TYPE_LOADING=1;
    ILoadMore LoadMore;
    boolean isLoading;
    int visibleThreshold=5;
    int lastVisibleItem,totalItemCount;

    public ItemAdapter(RecyclerView recyclerView,Context context, List<CompanyCommentListModel> datas) {
        this.context = context;
        this.datas = datas;

        final LinearLayoutManager linearLayoutManager=(LinearLayoutManager)recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount=linearLayoutManager.getItemCount();
                lastVisibleItem=linearLayoutManager.findLastCompletelyVisibleItemPosition();
                if(!isLoading && totalItemCount<=(lastVisibleItem+visibleThreshold)){
                    if(LoadMore!=null){
                        LoadMore.onLoadMore();
                        isLoading=true;
                    }
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return datas.get(position)==null?VIEW_TYPE_LOADING:VIEW_TYPE_ITEM;
    }

    public void setLoadMore(ILoadMore LoadMore) {
        this.LoadMore = LoadMore;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if(viewType==VIEW_TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.company_comment_list_item, parent, false);
            return new ItemHolder(itemView,context);
        }
        else if(viewType==VIEW_TYPE_LOADING){
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof ItemHolder){
            CompanyCommentListModel data=datas.get(position);
            ItemHolder itemHolder= (ItemHolder) holder;
            itemHolder.commentDate.setText(data.getCommentDate());
            itemHolder.ratingBar.setRating(Float.valueOf(data.getCommentRate()));
            itemHolder.commentText.setText(data.getCommentText());

        }else if(holder instanceof LoadingViewHolder){
            LoadingViewHolder loadingViewHolder= (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void setLoaded() {
        isLoading = false;
    }
}
