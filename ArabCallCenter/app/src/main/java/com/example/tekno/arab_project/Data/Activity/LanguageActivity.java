package com.example.tekno.arab_project.Data.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tekno.arab_project.Data.Util;
import com.example.tekno.arab_project.R;

import java.util.Locale;

public class LanguageActivity extends AppCompatActivity {
    private Util util;
    private Toolbar toolbar;
    private Button btn_english,btn_arabic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        util = new Util(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView toolbar_title=(TextView)toolbar.findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(util.getTypeface());
        toolbar_title.setText(getResources().getString(R.string.language));
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_home);

        btn_english=(Button)findViewById(R.id.btn_english);
        btn_english.setTypeface(util.getTypeface());
        btn_arabic=(Button)findViewById(R.id.btn_arabic);
        btn_arabic.setTypeface(util.getTypeface());
        btn_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locale locale = new Locale("");  //locale en yaptık. Artık değişkenler values-en paketinden alınacak
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
                finish();//mevcut acivity i bitir.
                Intent intent=new Intent(LanguageActivity.this,MainActivity.class);
                startActivity(intent);
                finish();//activity i baştan yükle
            }
        });

        btn_arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locale locale = new Locale("ar");  //locale en yaptık. Artık değişkenler values-en paketinden alınacak
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
                finish();//mevcut acivity i bitir.
                Intent intent=new Intent(LanguageActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
